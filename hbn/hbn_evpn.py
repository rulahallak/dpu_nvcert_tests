#!/usr/bin/env python3

##
## Copyright © 2023 NVIDIA CORPORATION & AFFILIATES. ALL RIGHTS RESERVED.
##
## This software product is a proprietary product of Nvidia Corporation and its affiliates
## (the "Company") and all right, title, and interest in and to the software
## product, including all associated intellectual property rights, are and
## shall remain exclusively with the Company.
##
## This software product is governed by the End User License Agreement
## provided with the software product.
##
## Author: Anuradha Karuppiah <anuradhak@nvidia.com>

# pylint: disable=attribute-defined-outside-init

"""
Configures an EVPN VxLAN bridge between DPU-1 and DPU-2
"""

try:
    import ipaddress
    import json
    import logging
    import time
    import hbn_node
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc

logger = logging.getLogger("dpu_nvcert")


class Vtep(hbn_node.HbnContainer):
    """
    Methods for managing a VxLAN tunnel endpoint
    """
    _MAX_WAIT_TIME = 30
    _VLAN_PIP_BASE = 20
    _VLAN_PHY_HW_ADDRESS_PREFIX = "00:01:00:00:1e:"
    _VLAN_PHY_HW_ADDRESS_BASE = 0
    _VLAN_VIRT_HW_ADDRESS = "00:00:5e:00:01:01"
    _BGP_PEER_GROUP_NAME = "fabric"
    _LO_IP_ADDR_BASE = 20
    _LO_IP_ADDR_PREFIX = "6.0.0."
    _BGP_ASN_BASE = 650020
    _VIDS = [111]
    _VID_TO_VNI = {111: 1000111}
    _VID_TO_PREFIX = {111: 60}


    def __init__(self, **kwargs):
        super().__init__(**kwargs, enable_frr = True)
        self.vxlan = None
        self.bridge = None
        self.vrf = None
        self.lo_rmap = None

        self.__add_lo_addr()
        self.__enable_daemons()


    def __add_lo_addr(self):
        """
        populate loopback ip address using node idx
        """
        lo_addr = self._LO_IP_ADDR_BASE + self.idx
        lo_addr_string = f"{self._LO_IP_ADDR_PREFIX}{lo_addr}/32"
        self.lo_iface.addrs.append(ipaddress.IPv4Interface(lo_addr_string))


    def __enable_daemons(self):
        """
        Enables relevant FRR daemons
        """
        self.daemons.append("bgpd")


    def __config_route_maps(self):
        """
        Create a routemap to permit loopback addresses
        """
        rmap = hbn_node.RouteMap("ALLOW_LO")
        self.lo_rmap = rmap
        self.rmaps.append(rmap)

        rule = hbn_node.RouteMapRule(10, "permit")
        rmap.rules.append(rule)
        rule.match_list.append("interface lo")

    def __config_bgp_router(self):
        """
        Create the BGP router for EVPN peering
        """

        self.__config_route_maps()

        asn = self._BGP_ASN_BASE + self.idx
        self.bgp_router = hbn_node.BgpRouter(asn,
                router_id = self.lo_iface.addrs[0].ip, vrf = "default")
        # template expects this dup info (fixme)
        self.asn = asn

        # Create a common peer group with datacenter settings
        peer_group = hbn_node.BgpPeerGroup(self._BGP_PEER_GROUP_NAME)
        self.bgp_router.peer_groups.append(peer_group)

        # create a bgp unnumbered neighbor for each uplink
        neighbors = []
        for iface in self.ifaces:
            if hasattr(iface, "uplink") and iface.uplink:
                neighbor = hbn_node.BgpNeighbor(iface.name, peer_group = peer_group)
                neighbors.append(neighbor)
        self.bgp_router.neighbors.extend(neighbors)

        # populate ipv4-unicast address-family
        # all neighbor are activated by default in the family (by FRR)
        address_family = hbn_node.BgpAf(hbn_node.BGP_AF_IPV4_UNICAST)
        self.bgp_router.afs.append(address_family)
        # redistribute the loopack network
        address_family.redistribute["connected"] = self.lo_rmap

        # populate l2vpn-evpn address-family
        address_family = hbn_node.BgpAf(hbn_node.BGP_AF_L2VPN_EVPN)
        self.bgp_router.afs.append(address_family)
        # activate the uplink neighbors
        for neighbor in neighbors:
            address_family.neighbors.append(neighbor)


    def __add_vrf(self):
        """
        create tenant vrf
        """
        self.vrf = hbn_node.Vrf(0, name = "vrf1")
        self.ifaces.append(self.vrf)


    def __add_vxlan(self):
        """
        create a SVD and setup the VLAN to VNI mapping
        """
        self.vxlan = hbn_node.Vxlan(0, name = "vxlan_default")
        self.ifaces.append(self.vxlan)
        self.vxlan.vid_to_vni = self._VID_TO_VNI.copy()

        # setup the tunnel ip address on the loopback
        self.lo_iface.tunnel_addr = self.lo_iface.addrs[0].ip

        # add the vxlan device as a bridge member
        self.bridge.ifaces.append(self.vxlan)


    def __add_bridge(self):
        """
        Create the default VLAN aware bridge with host representors and
        SVD
        """
        self.bridge = hbn_node.Bridge(0, name = "br_default")
        self.ifaces.append(self.bridge)
        for iface in self.ifaces:
            if hasattr(iface, "representor") and iface.representor:
                self.bridge.ifaces.append(iface)
        self.bridge.vids = self._VIDS.copy()
        self.bridge.access_vid = self._VIDS[0]


    def __add_svi(self, vid):
        """
        SVI is created per-VLAN with an anycast virtual IP and an individual
        physical IP

        """
        name = f"vlan{vid}"
        vlan = hbn_node.Vlan(0, vid = vid, name = name)
        self.ifaces.append(vlan)

        vlan.bridge = self.bridge

        # setup phy ip and HW addresses
        pip = self._VLAN_PIP_BASE + self.idx
        ip_prefix = self._VID_TO_PREFIX[vid]
        pip_v4 = f"{ip_prefix}.1.1.{pip}/24"
        vlan.addrs.append(ipaddress.IPv4Interface(pip_v4))
        pip_v6 = f"20{ip_prefix}:1:1:1::{pip}/64"
        vlan.addrs.append(ipaddress.IPv6Interface(pip_v6))
        vlan.hwaddress = self._VLAN_PHY_HW_ADDRESS_PREFIX +\
                f"{(self._VLAN_PHY_HW_ADDRESS_BASE + self.idx):02d}"

        # setup virtual ip and HW addresses
        pip_v4 = f"{ip_prefix}.1.1.250/24"
        vlan.virt_ipv4_addr = (ipaddress.IPv4Interface(pip_v4))
        pip_v6 = f"20{ip_prefix}:1:1:1::250/64"
        vlan.virt_ipv6_addr = (ipaddress.IPv6Interface(pip_v6))
        vlan.virt_hw_addr = self._VLAN_VIRT_HW_ADDRESS

        # attach the SVI to the tenant VRF
        vlan.vrf = self.vrf

    def __add_svis(self):
        """
        Creates SVIs for enabling ARP supression
        """
        for vid in self._VIDS:
            self.__add_svi(vid)


    def __add_vxlan_bridge(self):
        """
        Create the linux bridge with the vxlan device
        """
        # Create the tenant vrf
        self.__add_vrf()

        # Create the bridge device
        self.__add_bridge()

        # Create the vxlan device
        self.__add_vxlan()

        # Create the SVIs
        self.__add_svis()


    def __config_all(self):
        """
        Create the e/n/i and frr config needed for the VxLAN bridge
        """
        self.__add_vxlan_bridge()
        self.__config_bgp_router()

        # render the config into flat files and apply
        self.re_render_config_all()
        self.apply_config_all()


    def setup(self):
        """
        Configure the VTEP and check status of the hBN daemons
        """
        self.__config_all()

        ready = False

        logger.debug("Finished EVPN configuration on %s", self.oob_address)
        # wait for the services to become ready
        count = 0
        while count < 3:
            count += 1
            time.sleep(self._MAX_WAIT_TIME)
            ready = self.check_node_status()
            if ready:
                break
        return ready


    def cleanup(self):
        """
        fixme
        """

    def _check_macs(self, test_macs):
        """
        check if the test_mac entries are present in zebra
        """
        vni = self._VID_TO_VNI.get(self.bridge.access_vid, 0)
        cmd = f"show evpn mac vni {vni} json"
        output = self.frr_cmd(cmd)
        output = json.loads(output)
        macs = output.get("macs", {})
        macs = list(macs.keys())

        return set(test_macs).issubset(set(macs))

    def check_macs(self, test_macs, backoff = 3, attempts = 3):
        """
        Check if the mac entries are present in zebra
        If check fails retry with backoff.
        """
        r_val = False
        while attempts:
            r_val = self._check_macs(test_macs)
            if r_val:
                break
            attempts = attempts - 1
            if attempts and backoff:
                time.sleep(backoff)

        return r_val
