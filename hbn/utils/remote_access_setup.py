#!/usr/bin/env python3

##
## Copyright © 2023 NVIDIA CORPORATION & AFFILIATES. ALL RIGHTS RESERVED.
##
## This software product is a proprietary product of Nvidia Corporation and its affiliates
## (the "Company") and all right, title, and interest in and to the software
## product, including all associated intellectual property rights, are and
## shall remain exclusively with the Company.
##
## This software product is governed by the End User License Agreement
## provided with the software product.
##
## Author: Anuradha Karuppiah <anuradhak@nvidia.com>

# pylint: disable=too-few-public-methods
# pylint: disable=too-many-function-args
# pylint: disable=too-many-arguments

"""
1. This modules identifies the network devices on the host associated with
the DPU.
2. The DPU OOB address is used for this purpose.
3. The DPU OOB address can be unknown the administrator and hence specified as
0.0.0.0. This module use the host-rshim-network to ssh into the DPU and
acquire the DPU OOB address.
"""

try:
    import logging
    import re
    import yaml
    from utils import node
except ImportError as e:
    raise ImportError (str(e) + "- import module missing") from e

logger = logging.getLogger("dpu_nvcert")


def get_pcie_devices_from_mst(host):
    """
    This command can be run on the host or the DPU and is used to get the
    list of Bluefield pcie devices
    """
    cmd = "mst start"
    host.sudo(cmd)

    cmd = "mst status -v |grep BlueField"
    lines = host.sudo(cmd)
    pcie_devices = []
    for line in lines.splitlines():
        pcie_id = line.split()[2]
        pcie_devices.append(pcie_id)

    # only send the first device for each card for e.g. 17:00.0, cc:00.0
    # are the devices sent in ths example -
    # BlueField3(rev:1)       /dev/mst/mt41692_pciconf0.1   17:00.1
    # BlueField3(rev:1)       /dev/mst/mt41692_pciconf0     17:00.0
    # BlueField2(rev:1)       /dev/mst/mt41686_pciconf0.1   cc:00.1
    # BlueField2(rev:1)       /dev/mst/mt41686_pciconf0     cc:00.0

    final_pcie_devices = []
    for pcie_id in pcie_devices:
        if pcie_id.endswith(".0"):
            final_pcie_devices.append(pcie_id)

    return final_pcie_devices


def get_serial_number_from_lspci(host, pcie_id):
    """
    Gets the serial number using lspci. This API can be used on the host or
    DPU
    """
    serial_number = "unknown"
    cmd = f"lspci -vv -s {pcie_id} |grep -i serial"
    sn_out = host.sudo(cmd)
    sn_pat = re.compile(r"\[SN\] Serial number: (?P<serial_number>\S+)")
    obj = sn_pat.search(sn_out)
    if not obj:
        return serial_number

    serial_number = obj.group("serial_number")

    return serial_number


def get_serial_number_from_dpu(dpu):
    """
    Locates the serial number of the network card
    """
    serial_number = "unknown"

    pcie_devices = get_pcie_devices_from_mst(dpu)

    if not pcie_devices:
        return serial_number

    pcie_id = pcie_devices[0]

    return get_serial_number_from_lspci(dpu, pcie_id)


def get_pcie_by_serial_number_from_host(host, serial_number):
    """
    Fetches the pcie_id associated with the network card
    """
    pcie_devices = get_pcie_devices_from_mst(host)
    for pcie_id in pcie_devices:
        host_serial_number = get_serial_number_from_lspci(host, pcie_id)
        if host_serial_number == serial_number:
            return pcie_id

    return "unknown"


def populate_net_dev(host_data, host, dpu_data=None, dpu=None):
    """
    Translates the uplink port number to the mlx_dev name
    """
    mlx_dev = host_data.get("mlx_dev", None)

    # if the user has specified the mlx device directly there is nothing else to do
    if mlx_dev:
        return

    serial_number = get_serial_number_from_dpu(dpu)
    dpu_data["serial_number"] = serial_number
    logger.info(f"dpu serial_number {serial_number}")
    pcie_id = get_pcie_by_serial_number_from_host(host, serial_number)
    host_data["pcie_id"] = pcie_id
    logger.info(f"default host pcie_id {pcie_id}")

    uplink_port = host_data.get("uplink_port", 0)
    logger.info(f"uplink port {uplink_port}")
    port_suffix = f".{uplink_port}"
    pcie_id = pcie_id.replace(".0", port_suffix)
    logger.info(f"target host pcie_id {pcie_id}")

    cmd = "mst status -v |grep BlueField"
    lines = host.sudo(cmd)
    out = ""
    for line in lines.splitlines():
        if pcie_id in str(line):
            out = str(line)

    # Sample output
    # BlueField2(rev:1)       /dev/mst/mt41686_pciconf0     ca:00.0
    # mlx5_4          net-ens7f0np0             1
    if out:
        mlx_dev = out.split()[3]
        host_data["mlx_dev"] = mlx_dev
        logger.info(f"host mlx_dev {mlx_dev}")


def setup_host(host_data):
    """
    Instantiate the host class for running commands on the host
    """
    oob_address = host_data.get("oob_address")
    uname = host_data.get("user")
    password = host_data.get("password")

    host = node.RemoteNode(oob_address=oob_address, uname=uname, password=password)
    return host


def populate_dpu_oob(host_data, host, dpu_data):
    """
    use the host rshim network to populate the DPU OOB address
    """
    if dpu_data.get("oob_address") != "0.0.0.0":
        return

    rshim_num = host_data.get("rshim_num", 0)
    cmd = f"sshpass -p {dpu_data.get('password')} ssh rshim{rshim_num} ip addr show oob_net0"
    output = host.sudo(cmd)
    addr_pat = re.compile(r"inet (?P<addr>\S+)")
    addr_info = addr_pat.findall(output.stdout)
    if addr_info:
        dpu_data["oob_address"] =addr_info[0].split("/")[0]


def check_nic_mode(host, host_data):
    mlx_dev = host_data.get("mlx_dev", None)
    if mlx_dev:
        cmd = f"mst status -v | grep {mlx_dev}"
        mst_dev = host.sudo(cmd).split()[1].strip()
        cmd = f"mlxconfig -d {mst_dev} q INTERNAL_CPU_OFFLOAD_ENGINE | grep INTERNAL_CPU_OFFLOAD_ENGINE"
        res = host.sudo(cmd)
        if res:
            res = res.split()[1].strip()
            return (res == "DISABLED(1)")
    return False


def copy_uplink_from_host_to_dpu(host_data, dpu_data):
    """
    Copy the zero based uplink portid from host to DPU
    """
    uplink_port = host_data.get("uplink_port", 0)
    dpu_data["uplink_port"] = uplink_port


def validate_test_setup(inventory):
    """
    Parse the inventory file and check if all the nodes are accessible from
    the test-server
    """
    with open(inventory, encoding = "utf-8") as host_f:
        data = yaml.load(host_f, Loader=yaml.loader.SafeLoader)

    dpu_data_1 = None
    dpu_data_2 = None

    try:
        x86_hosts_data = data["x86-hosts"]
        host_data_1 = x86_hosts_data["host_1"]
        host_data_2 = x86_hosts_data["host_2"]
        dpu_data = data["dpus"]
        dpu_data_1 = dpu_data["dpu_1"]
        dpu_data_2 = dpu_data["dpu_2"]
    except KeyError:
        return None

    host_1 = setup_host(host_data_1)
    host_2 = setup_host(host_data_2)

    nic_mode_1 = check_nic_mode(host_1, host_data_1)
    nic_mode_2 = check_nic_mode(host_2, host_data_2)
    # nic_mode_1 = nic_mode_2 = False

    dpu_1 = None
    dpu_2 = None

    if not nic_mode_1:
        # populate DPU OOB address if it doesn't exist
        populate_dpu_oob(host_data_1, host_1, dpu_data_1)
        # Instantiate the DPUs
        dpu_1 = setup_host(dpu_data_1)
        # Copy uplink port from host to DPU
        copy_uplink_from_host_to_dpu(host_data_1, dpu_data_1)

    if not nic_mode_2:
        populate_dpu_oob(host_data_2, host_2, dpu_data_2)
        dpu_2 = setup_host(dpu_data_2)
        copy_uplink_from_host_to_dpu(host_data_2, dpu_data_2)

    # populate network devices for running the tests
    populate_net_dev(host_data_1, host_1, dpu_data_1, dpu_1)
    populate_net_dev(host_data_2, host_2, dpu_data_2, dpu_2)

    # setup password less root access from the server to the host-nodes
    # if it doesn't already exist
    logger.info(data)

    return (data, nic_mode_1, nic_mode_2)
