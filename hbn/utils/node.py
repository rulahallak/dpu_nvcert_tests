#!/usr/bin/env python3

##
## Copyright © 2023 NVIDIA CORPORATION & AFFILIATES. ALL RIGHTS RESERVED.
##
## This software product is a proprietary product of Nvidia Corporation and its affiliates
## (the "Company") and all right, title, and interest in and to the software
## product, including all associated intellectual property rights, are and
## shall remain exclusively with the Company.
##
## This software product is governed by the End User License Agreement
## provided with the software product.
##
## Author: Anuradha Karuppiah <anuradhak@nvidia.com>

# pylint: disable=too-few-public-methods
# pylint: disable=too-many-function-args
# pylint: disable=too-many-arguments
# pylint: disable=too-many-instance-attributes

"""
Defines the methods for running commands on remote and local nodes
"""

try:
    import ipaddress
    import jinja2
    import logging
    import os
    import re
    import time
    from fabric2 import Connection, Config
    from fabric2.transfer import Transfer
    from invoke.exceptions import UnexpectedExit, CommandTimedOut
    from invoke import run, sudo
    from paramiko.ssh_exception import NoValidConnectionsError
except ImportError as e:
    raise ImportError (str(e) + "- import module missing") from e

logger = logging.getLogger("dpu_nvcert")


class NodeError(Exception):
    """
    Exception handling - node related failures
    """

class SSHConnectError(Exception):
    """
    Exception handling - ssh related failures
    """

class CommandExecutionError(Exception):
    """
    Exception handling - command failures
    """

class CommandTimeout(Exception):
    """
    Exception handling - command timeout
    """

class TestError(Exception):
    """
    Incorrect API usage in the test
    """


class AttrStr(str):
    """
    Fetch string
    """
    @property
    def stdout(self):
        """
        Define attribute output
        """
        return str(self)


class RemoteNode:
    """
    A device that needs to be accessed via ssh for configuring and running
    tests. Python fabric2 module is used for this remote access.
    """
    def __init__(self, **kwargs):
        self.idx = kwargs.pop("idx", 0)
        self.oob_address = kwargs.pop("oob_address", "")
        self.uname = kwargs.pop("uname", "root")
        self.password = kwargs.pop("password", None)
        self.node_string = self.uname + "@" + str(self.oob_address)
        self.config = None
        self.connect_kwargs = {}

        if self.password:
            self.connect_kwargs['look_for_keys'] = False
            self.connect_kwargs['password'] = self.password
            self.config = Config(
                    overrides={'sudo': {'password': self.password}}, lazy=True)


    def format_output(self, output):
        """
        Format the output into a string and store the metadata as
        attributes
        """
        filtered_out = AttrStr(output.stdout.rstrip("\n"))
        setattr(filtered_out, "return_code", output.return_code)
        setattr(filtered_out, "failed", output.failed)
        setattr(filtered_out, "succeeded", output.ok)
        setattr(filtered_out, "command", output.command)
        setattr(filtered_out, "stderr", output.stderr)

        return filtered_out


    def sudo(self, *args, **kwargs):
        """
        remote sudo-bash command
        """
        kwargs["warn"] = True
        with Connection(self.node_string,
                connect_kwargs=self.connect_kwargs, config=self.config) as conn:
            try:
                r_obj = conn.sudo(*args, password=self.password, **kwargs)
            except UnexpectedExit as un_exc:
                if args:
                    raise CommandExecutionError(
                            f"Got the following error trying to execute {args[0]}: {un_exc}")\
                    from un_exc
                raise CommandExecutionError(
                        f"'sudo' method failed with the error: {un_exc}") from un_exc
            except (CommandTimedOut, TimeoutError) as tout_exc:
                raise CommandTimeout(
                        f"Command Timed Out: {tout_exc}") from tout_exc
            except NoValidConnectionsError as conn_exc:
                raise SSHConnectError(
                        f"Unable to connect to {self.oob_address}: {conn_exc}") from conn_exc

        return self.format_output(r_obj)


    def run(self, *args, **kwargs):
        """
        remote bash command
        """
        with Connection(self.node_string,
                connect_kwargs=self.connect_kwargs, config=self.config) as conn:
            try:
                r_obj = conn.run(*args, **kwargs)
            except UnexpectedExit as un_exc:
                if args:
                    raise CommandExecutionError(
                            f"Got the following error trying to execute {args[0]}: {un_exc}")\
                            from un_exc
                raise CommandExecutionError(
                        f"'run' method failed with the error: {un_exc}") from un_exc
            except (CommandTimedOut, TimeoutError) as tout_exc:
                raise CommandTimeout(
                        f"Command Timed Out: {tout_exc}") from tout_exc
            except NoValidConnectionsError as conn_exc:
                raise SSHConnectError(
                        f"Unable to connect to {self.oob_address}: {conn_exc}") from conn_exc

        return self.format_output(r_obj)


    def put(self, local_file, remote_file, final_remote_file, **kwargs):
        """
        Copy file from the test server to the remote node
        """
        outside_container = kwargs.pop("outside_container", False)
        with Connection(self.node_string,
                connect_kwargs=self.connect_kwargs, config=self.config) as conn:
            try:
                Transfer(conn).put(local_file, remote_file, **kwargs)
            except (FileNotFoundError, OSError) as no_file:
                raise CommandExecutionError(
                        f"File not found error {no_file}") from no_file
            except UnexpectedExit as un_exc:
                raise CommandExecutionError(
                        f"File transfer failed, {remote_file}: {un_exc}") from un_exc

        if final_remote_file:
            cmd = f"cp {remote_file} {final_remote_file}"
            self.sudo(cmd, outside_container = outside_container)


class LocalNode:
    """
    Script is being run on this device i.e. script has local bash access
    """
    def __init__(self, **kwargs):
        pass


    def format_output(self, output):
        """
        Format command output and stash metadata
        """
        filtered_out = AttrStr(output.stdout.rstrip("\n"))
        setattr(filtered_out, "return_code", output.return_code)
        setattr(filtered_out, "failed", output.failed)
        setattr(filtered_out, "succeeded", output.ok)
        setattr(filtered_out, "command", output.command)
        setattr(filtered_out, "stderr", output.stderr)

        return filtered_out


    def sudo(self, command, **kwargs):
        """
        local sudo-bash command
        """
        return self.format_output(sudo(command, **kwargs))


    def run(self, command, **kwargs):
        """
        local bash command
        """
        return self.format_output(run(command, **kwargs))


class NetDev:
    """
    Properties of the host device used for the test
    """
    _INTERFACE_DIR = "/sys/class/net/"
    _INTERFACE_SPEED_DIR = "speed"
    _INTERFACE_HW_ADDR_DIR = "address"
    _IP_ADDR_PREFIX = "172.16"
    _IP_ADDR_NETMASK = "24"
    _TEMPLATES_DIR = "templates"
    _NETPLAN_TEMPLATE = "pf.conf.j2"
    _NETPLAN_FILE_NAME = "10-nvcert-config.yaml"


    def __init__(self, node, mlxdev):
        self.node = node
        self.mlxdev = mlxdev
        self.mlxdev_num = int(mlxdev.split("_")[1])
        self.name = None
        self.speed = None
        self.hwaddress = None
        self.ipv4_addrs = []

        self.test_server_files_dir =  f"/tmp/dpu_nvcert/host_{self.node.idx}/"
        # find the device and pull the properties
        self._get_netdev()


    def __str__(self):
        output = "\n"
        output += f"{self.name} {self.mlxdev} speed {self.speed} mac {self.hwaddress}\n"
        output += "    ipv4"
        for addr in self.ipv4_addrs:
            output += f" {str(addr.ip)}"
        output += "\n"

        return output


    def _parse_netdev_inet(self):
        output = self.node.sudo(f"ip addr show {self.name}")
        addr_pat = re.compile(r"inet (?P<addr>\S+)")

        return addr_pat.findall(output)


    def _add_netdev_inet(self):
        """
        Create a netplan config file with the ipv4 address
        """
        pf_name = self.name
        if self.node.uplink_network:
            pf_ipv4_address = self.node.uplink_network
        else:
            addr = self.node.idx
            pf_ipv4_address = f"{self._IP_ADDR_PREFIX}.{self.mlxdev_num}.{addr}/{self._IP_ADDR_NETMASK}"
        logger.debug("Add address %s to %s", pf_ipv4_address, pf_name)
        environment = jinja2.Environment(loader=jinja2.FileSystemLoader(self._TEMPLATES_DIR))
        template = environment.get_template(self._NETPLAN_TEMPLATE)
        content = template.render(pf_name=pf_name,
                pf_ipv4_address=pf_ipv4_address)

        # create the netplan config file on the test-server first
        path_exists = os.path.exists(self.test_server_files_dir)
        if not path_exists:
            # Create a new directory because it does not exist
            os.makedirs(self.test_server_files_dir)
        test_server_file_name =  self.test_server_files_dir +\
                                 self._NETPLAN_FILE_NAME
        with open(test_server_file_name, mode="w", encoding="utf-8") as conf_file:
            conf_file.write(content)

        # copy the netplan config file from the test-server to the
        # remote host
        host_tmp_location = "/tmp/"
        self.node.put(test_server_file_name, host_tmp_location, None)
        host_file_name =  "/etc/netplan/" + self._NETPLAN_FILE_NAME
        self.node.sudo(f"mv /tmp/{self._NETPLAN_FILE_NAME} {host_file_name}")
        self.node.sudo("netplan apply")

        return self._parse_netdev_inet()

    def _is_link_up(self):
        """
        Returns True if link is operationally up
        """
        logger.debug("Enable device %s", self.name)
        output = self.node.sudo(f"ip link show {self.name}")
        return "LOWER_UP" in output

    def _wait_for_link_up(self):
        """
        Waits for link up; backing off and checking carrier status
        periodically
        """
        attempts = 3
        while attempts:
            attempts = attempts - 1
            if self._is_link_up():
                return
            time.sleep(10)

    def _get_netdev(self):
        output = self.node.sudo("ibdev2netdev")
        dev_pat = re.compile(fr"{self.mlxdev} port 1 ==> (?P<name>\S+)")
        obj = dev_pat.search(output)
        if obj:
            self.name = obj.group("name")

        if not self.name:
            return

        # Admin up the netdev
        self.node.sudo(f"ip link set {self.name} up")
        self._wait_for_link_up()

        # speed
        output = self.node.sudo(
                f"cat {self._INTERFACE_DIR}{self.name}/{self._INTERFACE_SPEED_DIR}")
        self.speed = int(output)

        # mac
        output = self.node.sudo(
                f"cat {self._INTERFACE_DIR}{self.name}/{self._INTERFACE_HW_ADDR_DIR}")
        self.hwaddress = output

        # IPv4 addresses
        addrs = self._add_netdev_inet()

        for addr in addrs:
            self.ipv4_addrs.append(ipaddress.IPv4Interface(addr))


class HostNode(RemoteNode):
    """
    Server hosting the DPU
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.oob_address = kwargs.pop("oob_address", None)
        self.uplink_network = kwargs.pop("uplink_network", None)
        self.mlxdev = kwargs.pop("dev", None)
        self.netdev = NetDev(self, self.mlxdev)


    def __str__(self):
        return f"{self.oob_address} {self.netdev}"


class DpuNode(RemoteNode):
    """
    DPU (Arm subsystem)
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.mstdev = self._get_mstdev()
        self.privileged = self._is_dpu_level_privileged()
        # This is bit of a hack that simply identifies the number of mlx
        # devices
        out = super().sudo("mst status -v |grep BlueField")
        self.num_uplinks = len(out.splitlines())
        # uplink port being tested
        self.uplink_port = kwargs.pop("uplink_port", 0)

    def _get_mstdev(self):
        """
        Find mst device name on the DPU
        """
        super().sudo("mst start")
        return super().sudo("find /dev/mst/ | grep -G 'pciconf0$'")


    def _is_dpu_level_privileged(self):
        """
        Check the DPU level
        """
        level = super().sudo(f"mlxprivhost -d {self.mstdev} q |grep level")
        priv = False
        level_result = level.split(":")[1].strip()
        if level_result == "PRIVILEGED":
            priv = True

        logger.info("DPU is in %s mode", "privileged" if priv else "restricted")
        return priv


    def set_jumbo_mtu_on_uplinks(self):
        """
        Locate all the uplinks on the dpu and change their mtu to 9000
        """
        for i in range(self.num_uplinks):
            uplink = f"p{i}"
            cmd = f"ip link set {uplink} mtu 9216"
            logger.info(cmd)
            super().sudo(cmd)

            host_rep = f"pf{i}hpf"
            cmd = f"ip link set {host_rep} mtu 9216"
            logger.info(cmd)
            super().sudo(cmd)


    def _parse_crictl_ps_(self):
        """
        This method finds the mapping from to container name to container id.
        It returns a dictionary where the keys are the container names and
        the values are the container ids
        """
        data = {}
        output = super().sudo("crictl ps")
        lines = output.splitlines()
        container_idx = 0
        name_idx = 0
        len_check = 8
        for line in lines:
            info = [ e.replace("_", "").lstrip() for e in line.split("  ")  if e ]
            if info and re.search("[A-Z]+", info[0]):
                for i, eobj in enumerate(info):
                    if eobj == "CONTAINER":
                        container_idx = i
                        len_check = len(info)
                    elif eobj == "NAME":
                        name_idx = i
            else:
                if info and (len(info) == len_check):
                    data[info[name_idx]] = info[container_idx]
                elif info and (len(info) != 8 ):
                    raise NodeError("Unknown format from \"crictl\"")

        return data


    def get_container_id(self, container_name):
        """
        Maps a container name to container id
        """
        container_id = None
        containers = self._parse_crictl_ps_()

        cnames = list(containers.keys())
        for k in cnames:
            if container_name in k:
                container_id = containers.get(k)
                break

        return container_id


class DpuContainer(DpuNode):
    """
    Service container running on the DPU e.g. HBN
    """
    def __init__(self, container_name, **kwargs):
        super().__init__(**kwargs)
        self._container_id = None
        self.container_name = container_name
        self.dpu_files_dir = "/tmp/dpu_nvcert/" + container_name
        super().run(f"mkdir -p {self.dpu_files_dir}")
        logger.debug("Created directory for storing files %s", self.dpu_files_dir)


    @property
    def container_id(self):
        """
        Fetch the container id using the container name
        """
        if not self._container_id:
            self._container_id = self.get_container_id(self.container_name)

        return self._container_id


    def sudo(self, *args, **kwargs):
        """
        These commands are run within a container and need to be wrapped
        with crictl.
        """
        if "outside_container" in kwargs:
            run_outside_cont = kwargs.pop("outside_container")
            if run_outside_cont:
                return super().sudo(*args, **kwargs)

        return super().sudo(
                f"crictl exec {self.container_id} bash -c \"{args[0]}\"", **kwargs)
