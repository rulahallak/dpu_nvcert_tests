#!/usr/bin/env python3

##
## Copyright © 2023 NVIDIA CORPORATION & AFFILIATES. ALL RIGHTS RESERVED.
##
## This software product is a proprietary product of Nvidia Corporation and its affiliates
## (the "Company") and all right, title, and interest in and to the software
## product, including all associated intellectual property rights, are and
## shall remain exclusively with the Company.
##
## This software product is governed by the End User License Agreement
## provided with the software product.
##
## Author: Anuradha Karuppiah <anuradhak@nvidia.com>

# pylint: disable=too-few-public-methods
# pylint: disable=too-many-function-args
# pylint: disable=too-many-instance-attributes

"""
Methods for configuring a HBN (Host-Based-Networking) container using
e/n/i and frr.conf
"""

try:
    import logging
    import os
    import re
    from mako.template import Template
    from utils import node
except ImportError as e:
    raise ImportError (str(e) + "- import module missing") from e

logger = logging.getLogger("dpu_nvcert")


class Iface:
    """
    Interface definition
    """
    def __init__(self, *args, **kwargs):
        args = list(args)
        if len(args) < 1:
            raise node.TestError("Index option is missing")

        self.index = list(args).pop(0)
        self.name = kwargs.pop("name", f"name-{self.index}")
        self.add_empty = False
        self.addrs = []
        self.ipv4_addrs = []
        self.ipv6_addrs = []
        self.autoneg = None
        self.duplex = None
        self.speed = None
        self.mtu = None

        self.cmds = []
        self.zebra_cmds = []
        self.bgp_cmds = []
        self.hwaddress = None


    def __str__(self):
        return self.name


    def render_eni(self):
        """
        populate the strings needed for rendering /etc/network/interfaces file
        """
        if self.hwaddress:
            self.cmds.append(f"hwaddress {self.hwaddress}")
        if self.mtu:
            self.cmds.append(f"mtu {self.mtu}")


    def clear_eni(self):
        """
        clear the eni related strings
        """
        # reset all the eni commands
        self.cmds = []


    def render_frr_conf(self):
        """
        populate the cmd strings needed for rendering /etc/frr/frr.conf file
        """


    def clear_frr_conf(self):
        """
        clear FRR related strings
        """
        self.zebra_cmds = []
        self.bgp_cmds = []


class Loopback(Iface):
    """
    Mandatory loopback device
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tunnel_addr = None


    def render_eni(self):
        """
        populate the strings needed for rendering /etc/network/interfaces file
        """
        super().render_eni()
        if self.tunnel_addr:
            self.cmds.append(f"vxlan-local-tunnelip {self.tunnel_addr}")


class Vrf(Iface):
    """
    Vrf device with automatic table-id option
    """
    def __init__(self, *args, **kwargs):
        self.table = kwargs.pop("table", None)
        super().__init__(*args, **kwargs)


    def render_eni(self):
        """
        populate the strings needed for rendering /etc/network/interfaces file
        """
        super().render_eni()
        if not self.table:
            self.cmds.append("vrf-table auto")
        elif 1000 < self.table < 1255:
            self.cmds.append("vrf-table {self.table}")
        else:
            raise Exception("Invalid table id {self.table}, must value between 1001 and 1255")


class Vxlan(Iface):
    """
    Vxlan device
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.vid_to_vni = {}


    def render_eni(self):
        """
        populate the strings needed for rendering /etc/network/interfaces file
        """
        super().render_eni()
        vlan_vni_map = [str(vid) + "=" + str(vni)
            for (vid, vni) in self.vid_to_vni.items()]
        vlan_vni_map = " ".join(vlan_vni_map)
        self.cmds.append(f"bridge-vlan-vni-map {vlan_vni_map}")


class Bridge(Iface):
    """
    Bridge (ports, vids)
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ifaces = []
        self.vids = []
        #default PVID is 1
        self.pvid = 1
        self.access_vid = 0


    def render_eni(self):
        """
        populate the strings needed for rendering /etc/network/interfaces file
        """
        super().render_eni()
        self.cmds.append("bridge-vlan-aware yes")

        # add bridge members
        br_ports = []
        for iface in self.ifaces:
            br_ports.append(iface.name)
            if self.access_vid and not isinstance(iface, Vxlan):
                iface.cmds.append(f"bridge-access {self.access_vid}")
        br_port_str = " ".join(br_ports)
        self.cmds.append(f"bridge-ports {br_port_str}")

        vids = [str(vid) for vid in self.vids]
        br_vid_str = " ".join(vids)
        self.cmds.append(f"bridge-vids {br_vid_str}")
        self.cmds.append(f"bridge-pvid {self.pvid}")


class Vlan(Iface):
    """
    SVI with raw_device=parent-bridge
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.vid = kwargs.pop("vid", None)
        self.bridge = None
        self.vrf = None
        self.virt_hw_addr = None
        self.virt_ipv4_addr = None
        self.virt_ipv6_addr = None


    def render_eni(self):
        """
        populate the strings needed for rendering /etc/network/interfaces file
        """
        super().render_eni()
        self.cmds.append(f"vlan-id {self.vid}")
        self.cmds.append(f"vlan-raw-device {self.bridge.name}")
        self.cmds.append(f"vrf {self.vrf.name}")
        self.cmds.append(
                f"address-virtual {self.virt_hw_addr} {self.virt_ipv6_addr} {self.virt_ipv4_addr}")


class BgpPeerGroup:
    """
    BGP neighbors that share the same property
    """

    def __init__(self, name, is_ebgp = True):
        self.name = name
        self.is_ebgp = is_ebgp
        # advertise IPv4 Prefixes with IPv6 nexthops
        self.extended_nexthop = True

    def render_frr_conf(self, bgp_router_cmds):
        """
        populate the cmd strings needed for rendering /etc/frr/frr.conf file
        """
        bgp_router_cmds.append(f"neighbor {self.name} peer-group")
        if self.is_ebgp:
            bgp_router_cmds.append(f"neighbor {self.name} remote-as external")
        else:
            bgp_router_cmds.append(f"neighbor {self.name} remote-as internal")
        if self.extended_nexthop:
            bgp_router_cmds.append(f"neighbor {self.name} capability extended-nexthop")


class BgpNeighbor:
    """
    BGP neighbor
    """

    def __init__(self, name, peer_group):
        self.name = name
        self.peer_group = peer_group

    def render_frr_conf(self, bgp_router_cmds):
        """
        populate the cmd strings needed for rendering /etc/frr/frr.conf file
        """
        bgp_router_cmds.append(f"neighbor {self.name} interface peer-group {self.peer_group.name}")


BGP_AF_IPV4_UNICAST = 1
BGP_AF_IPV6_UNICAST = 2
BGP_AF_L2VPN_EVPN = 3

class BgpAf:
    """
    Config common to all address families
    """
    _AF_ID2STR = {BGP_AF_IPV4_UNICAST : "ipv4 unicast",
                  BGP_AF_IPV6_UNICAST : "ipv6 unicast",
                  BGP_AF_L2VPN_EVPN : "l2vpn evpn"}

    def __init__(self, address_family):
        # {protocol: routemap}
        # example: "connected" : "route-map=all"
        self.address_family = address_family
        self.redistribute = {}
        self.neighbors = []

    def render_frr_conf(self, bgp_router_cmds):
        """
        populate the cmd strings needed for rendering /etc/frr/frr.conf file
        """
        bgp_router_cmds.append(f"address-family {self._AF_ID2STR[self.address_family]}")

        if self.address_family == BGP_AF_L2VPN_EVPN:
            bgp_router_cmds.append("advertise-all-vni")

        for neighbor in self.neighbors:
            bgp_router_cmds.append(f"neighbor {neighbor.name} activate")

        for (proto, rmap) in self.redistribute.items():
            if rmap:
                bgp_router_cmds.append(f"redistribute {proto} route-map {rmap.name}")
            else:
                bgp_router_cmds.append(f"redistribute {proto}")

        bgp_router_cmds.append("exit-address-family")


class BgpRouter:
    """
    BGP neighbor
    """
    def __init__(self, asn, router_id, is_ebgp = True, vrf = "default"):
        self.peer_groups = []
        self.neighbors = []
        self.asn = asn
        self.is_ebgp = is_ebgp
        self.vrf = vrf
        self.router_id =  router_id
        self.afs = []


    def render_frr_conf(self, bgp_router_cmds):
        """
        populate the cmd strings needed for rendering /etc/frr/frr.conf file
        """
        if not self.asn:
            return

        bgp_router_cmds.append("! Router bgp")

        # add bgp router
        bgp_router_cmds.append(f"router bgp {self.asn} vrf {self.vrf}")
        bgp_router_cmds.append(f"bgp router-id {self.router_id}")

        # enable multipath-relax for allowing ecmp with ebgp
        if self.is_ebgp:
            bgp_router_cmds.append("bgp bestpath as-path multipath-relax")

        # add peer groups
        bgp_router_cmds.append("! Neighbors")
        for peer_group in self.peer_groups:
            peer_group.render_frr_conf(bgp_router_cmds)

        # add neighbors
        for neighbor in self.neighbors:
            neighbor.render_frr_conf(bgp_router_cmds)

        # add address families
        bgp_router_cmds.append("! Address Families")
        for address_family in self.afs:
            address_family.render_frr_conf(bgp_router_cmds)

        bgp_router_cmds.append(f"! end of router bgp {self.asn} vrf {self.vrf}")


class RouteMapRule:
    """
    This routemap definition is woefully incomplete (fixme)
    """
    def __init__(self, seq, rule_type = "permit"):
        self.type = rule_type
        self.seq = seq
        self.match_list = []


    def render_frr_conf(self, bgp_router_cmds):
        """
        populate the cmd strings needed for rendering /etc/frr/frr.conf file
        """
        for match in self.match_list:
            bgp_router_cmds.append(f"match {match}")


class RouteMap:
    """
    This routemap definition is woefully incomplete (fixme)
    """
    def __init__(self, name):
        self.name = name
        self.rules = []


    def render_frr_conf(self, bgp_router_cmds):
        """
        populate the cmd strings needed for rendering /etc/frr/frr.conf file
        """
        for rule in self.rules:
            bgp_router_cmds.append(f"route-map {self.name} {rule.type} {rule.seq}")
            rule.render_frr_conf(bgp_router_cmds)


class HbnContainer(node.DpuContainer):
    """
    Methods for configuring the HBN DOCA service
    """
    _ACCESS_VLAN_BASE = 111
    _TEMPLATES_DIR = "hbn/templates"
    _INTERFACES_FILE = "interfaces"
    _DAEMONS_FILE = "daemons"
    _FRR_CONF = "frr.conf"
    _JUMBO_MTU = 9218

    def __init__(self, **kwargs):
        super().__init__(container_name="hbn", **kwargs)
        self.name = kwargs.pop("name", None)
        self.enable_frr = kwargs.pop("enable_frr", False)
        self.__add_lo()
        self.ifaces = []
        self.__add_base_interfaces()
        self.bgp_router = None
        self.asn = None
        self.rmaps = []

        # config
        self.eni_dir = "/etc/network/interfaces"
        self.frr_dir = "/etc/frr"
        self.test_server_files_dir =  f"/tmp/dpu_nvcert/hbn/{self.name}"
        self.daemons = []
        self.zebra_cmds = []
        self.bgp_cmds = []
        self.bgp_router_cmds = []
        self.container_mount = "/var/lib/hbn/etc"
        self.remote_etc_path = {self._INTERFACES_FILE : "network/interfaces",
                                self._FRR_CONF : "frr/frr.conf",
                                self._DAEMONS_FILE : "frr/daemons"}


    def __str__(self):
        info_str = ""
        info_str += f"Loopback: idx {self.lo_iface.index} name {self.lo_iface.name}\n"
        info_str += "Interfaces: \n"
        for iface in self.ifaces:
            info_str += f"  idx {iface.index} name {iface.name}\n"
        return info_str


    def __write_config(self, config_file, **kwargs):
        """
        Render config file using the corresponding mako template
        """
        temp = Template(filename=f"{self._TEMPLATES_DIR}/{config_file}.tmpl")

        filename = f"{self.test_server_files_dir}/{config_file}"

        path_exists = os.path.exists(self.test_server_files_dir)
        if not path_exists:
            # Create a new directory because it does not exist
            os.makedirs(self.test_server_files_dir)

        with open(filename, "w+", encoding="utf-8") as cfg_file:
            content = temp.render(node=self, **kwargs)
            cfg_file.write(content)
        logger.debug("Updated config file %s", filename)


    def __copy_config(self, config_file):
        """
        upload the config file to the network device
        """
        filename = f"{self.test_server_files_dir}/{config_file}"
        remote = f"{self.dpu_files_dir}/{config_file}"
        final_remote = f"{self.container_mount}/{self.remote_etc_path[config_file]}"
        logger.debug("Copy %s to %s and %s on network device",
                filename, remote, final_remote)
        self.put(filename, remote, final_remote, outside_container=True)


    def __add_frr_cumulus_defaults(self):
        """
        populate default frr configuration
        """
        # add datacenter defaults
        self.zebra_cmds.append("! Cumulus Defaults")
        self.zebra_cmds.append("frr defaults datacenter")
        # leave logging as informational for now
        self.zebra_cmds.append("log syslog informational")
        # is this specific to DPUs? (fixme)
        self.zebra_cmds.append("no zebra nexthop kernel enable")


    def __add_lo(self):
        """
        Create the mandatory lo device
        """
        self.lo_iface = Loopback(0, name = "lo")


    def __add_host_pf(self, idx):
        """
        Host PF representative
        """
        iface = Iface(idx, name = f"pf{idx}hpf_sf")
        setattr(iface, "representor", 1)
        iface.add_empty = True
        iface.mtu = self._JUMBO_MTU
        self.ifaces.append(iface)


    def __is_host_pf(self, iface):
        """
        True if interface is a host port representor e.g. pf0hpf
        """
        return hasattr(iface, "representor") and iface.representor


    def __add_host_pfs(self):
        """
        Host PF config
        """
        for idx in range(self.num_uplinks):
            self.__add_host_pf(idx)


    def __add_uplink(self, idx):
        """
        Uplinks p0/p1
        """
        iface = Iface(idx, name = f"p{idx}_sf")
        setattr(iface, "uplink", 1)
        iface.add_empty = True
        iface.mtu = self._JUMBO_MTU
        self.ifaces.append(iface)


    def __is_uplink(self, iface):
        """
        True if interface is an uplink e.g. p0/p1
        """
        return hasattr(iface, "uplink") and iface.uplink


    def __add_uplinks(self):
        """
        Uplink representatives
        """
        self.__add_uplink(self.uplink_port)


    def __add_base_interfaces(self):
        """
        Add the host representors and uplink devices
        """
        self.__add_host_pfs()
        self.__add_uplinks()


    def render_frr_conf(self):
        """
        populate the cmd strings needed for rendering /etc/frr/frr.conf file
        """
        if not self.enable_frr:
            return

        self.__add_frr_cumulus_defaults()
        self.bgp_router.render_frr_conf(self.bgp_router_cmds)

        for rmap in self.rmaps:
            rmap.render_frr_conf(self.bgp_router_cmds)

        self.lo_iface.clear_frr_conf()
        for iface in self.ifaces:
            iface.render_frr_conf()


    def clear_frr_conf(self):
        """
        clear FRR related strings
        """
        self.zebra_cmds = []
        self.bgp_cmds = []
        self.bgp_router_cmds = []

        self.lo_iface.clear_frr_conf()
        for iface in self.ifaces:
            iface.clear_frr_conf()


    def re_render_frr_conf(self):
        """
        Clear the eni related strings in each interface and re-render it using
        current state
        """
        self.clear_frr_conf()
        self.render_frr_conf()


    def re_render_eni(self):
        """
        Clear the eni related strings in each interface and re-render it using
        current state
        """
        self.lo_iface.clear_eni()
        for iface in self.ifaces:
            iface.clear_eni()

        self.lo_iface.render_eni()
        for iface in self.ifaces:
            iface.render_eni()


    def re_render_config_all(self, **kwargs):
        """
        Update eni, frr/daemons, frr.conf. This is a two step process -
        1. Files are first rendered from the runtime attributes and stored
           on the TestServer (per-node)
        2. Rendered files are transferred to the remote node
        """
        self.re_render_eni()
        self.re_render_frr_conf()
        self.__write_config(self._INTERFACES_FILE, **kwargs)

        self.__write_config(self._DAEMONS_FILE, **kwargs)
        self.__write_config(self._FRR_CONF, **kwargs)


        self.__copy_config(self._INTERFACES_FILE)
        self.__copy_config(self._DAEMONS_FILE)
        self.__copy_config(self._FRR_CONF)
        logger.debug("%s", self)


    def set_mtu(self):
        """
        Set a mtu of 9218 on the host representors and uplinks to
        accomodate the vxlan header
        """
        for iface in self.ifaces:
            if self.__is_host_pf(iface) or self.__is_uplink(iface):
                real_name = iface.name.replace("_sf", "")
                self.sudo(f"ip link set dev {real_name} mtu {self._JUMBO_MTU}")


    def apply_config_all(self):
        """
        Apply the config files
        """
        self.reload_ifupdown2()
        #self.set_mtu()
        # It maybe possible to replace this steps with the less
        # impactful reload_frr
        if self.enable_frr:
            self.restart_frr()
        else:
            self.stop_frr()

        # This step is not needed
        self.restart_nl2doca()


    def reload_ifupdown2(self):
        """
        Apply e/n/i changes
        """
        self.sudo("ifreload -a")


    def reload_frr(self):
        """
        Apply FRR changes
        """
        self.sudo("/usr/lib/frr/frrinit.sh reload")


    def restart_frr(self):
        """
        Restart the routing daemon
        """
        self.sudo("supervisorctl restart frr")


    def stop_frr(self):
        """
        Restart the routing daemon
        """
        self.sudo("supervisorctl stop frr")


    def restart_nl2doca(self):
        """
        Restart nl2doca; the services that acclerates the kernel forwarding
        databases
        """
        self.sudo("supervisorctl restart nl2doca")


    def check_daemon_status(self, daemon_name):
        """
        Check if a HBN process is running
        """
        output = self.sudo("supervisorctl status " + daemon_name)

        state = False

        for line in output.stdout.splitlines():
            info = re.search(r"(\S+)\s+(\S+).*", line)

            if info:
                process_name = info.group(1)
                state = info.group(2)

                if (process_name == daemon_name) and (state == "RUNNING"):
                    state = True

        return state


    def check_frr_status(self):
        """
        Check FRR status
        """
        return self.check_daemon_status("frr")


    def check_nl2doca_status(self):
        """
        Check nl2doca status
        """
        nl2doca_state = self.check_daemon_status("nl2doca")
        if not nl2doca_state:
            return nl2doca_state

        # even if nl2doca service is running the child daemon nl2docad
        # may have exited silently. check if the daemon is still
        # there.
        nl2doca_pid = self.sudo("pidof nl2docad")
        if nl2doca_pid:
            print(f"nl2docad_pid {nl2doca_pid}")
        else:
            nl2doca_state = False

        return nl2doca_state

    def check_node_status(self):
        """
        Check if a HBN daemon is running
        """
        if self.enable_frr and not self.check_frr_status():
            return False
        if not self.check_nl2doca_status():
            return False

        return True


    def frr_cmd(self, cmd):
        """
        Run a cmd within the FRR vty shell
        """
        return self.sudo(f"vtysh -c '{cmd}'")
