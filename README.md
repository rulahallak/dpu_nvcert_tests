# NVIDIA Certification for Bluefield DPUs
This is a collection of performance tests that can be used to certify a server with a NVIDIA Bluefield DPU. The tests are organized into two modules -
- Host Based Networking (HBN) Tests - [hbn/README.md](https://gitlab.com/nvidia/networking/bluefield/nvcert/dpu_nvcert_tests/-/blob/main/hbn/README.md)
- Multinode Performance Tests - This is a git sub module that provides provides OVS, RDMA and IPSec tests - [multinode_perf/README.md](https://github.com/Mellanox/ngc_multinode_perf)

## Topology
![alt text](<./docs/Topology-with-test-server.jpg>)

## Test setup requirements
The DPU uplinks (p0) need to be connected back-to-back or to a top-of-rack (TOR) switch. The DPU uplinks are used in `Ethernet` mode. If you are using a TOR for the connection ensure that the switch ports on the TOR have matching speeds and are provisioned with the same VLAN to allow traffic bridging between the DPUs. The tests do not require access to the TOR. High throughput test traffic with varying patterns is sent over this (blue) network hence it is important to isolate it from the rest of your management network. Using a separate access (untagged/PVID) VLAN on the TOR for the DPU uplinks should suffice.

You also need out-of-band ssh access from the test server to the two x86-hosts and the two DPUs for installing, configuring and running the tests.

The DPU’s OOB 1G-port (oob_net0) needs to be cabled to a management network with a DHCP-server. The DPU (DHCP assigned) OOB IPv4 address is used for installing software and for configuring the DPU.

If the certification is done in `Zero Trust` mode the DPU needs to have a BMC for DPU management. You can check if the DPU features an integrated BMC by running the following command on the host -
```
root@x86-host:~# lspci -vv -s 17:00.0 |grep -i "product name"
                Product Name: BlueField-3 P-Series DPU 200GbE/NDR200 VPI dual-port QSFP112, PCIe Gen5.0 x16, Crypto Enabled, 32GB on-board DDR, BMC, FHHL
root@x86-host:~#
```
The DPU OOB and DPU BMC share the same external 1Gbps port. Separate MAC addresses are allocated to the DPU OOB and BMC ports. The DPU (DHCP assigned) BMC IPv4 address is used for installing the DPU OS.

Internet access is required in the OOB network for downloading packages.

## Host Operating System
The x86-host needs to run Ubuntu22.04 (Jammy Jellyfish). Installing the host OS is outside the scope of the DPU NvCert ansible playbooks.<br>
If you have any pre-installed version of the Mellanox OFED drivers on the host, you need to remove them by running `sudo /usr/sbin/ofed_uninstall.sh` before installing the DPU packages. This step is not required if you did a clean install of Ubuntu 22.04 iso.

## Onboarding new DPUs
Out-of-the-box DPUs can be onboarded in `Host Trusted` mode or in `Zero Trust` mode.

### Host Trusted Mode
By default, DPU is in the embedded/ECPF mode where the host is trusted. Host can ssh into the DPU using the host-rshim network, change NIC config and program the DPU's acceleration engines.

To onboard a new DPU in `Host Trusted` mode you cable the 1G OOB port to a management network with a DHCP server and run the ansible installation-playbook described in the next section. The installation-playbook uses the host rshim network for DPU OS installation and for querying the (DHCP assigned) DPU OOB IPv4 address.

### Zero Trust Mode
Sample DPU Board Label -
<img src=./docs/board-label.jpg alt="DPU sticker" width=50% height=50% title="DPU Title">

To onboard a new DPU in `Zero Trust` mode you need to -
- Locate the DPU BMC MAC (`DPU BMC:`) and DPU OOB MAC (`OOB:`) on the DPU `Board Label`
- Setup MAC to IPv4 mappings for the DPU BMC MAC and DPU OOB MAC on the DHCP server running in the management network.
- The DPU BMC and DPU OOB share the same external port. Cable this port to the 1Gbps management network.
- Run the ansible zero-trust installation playbook. The playbook installs the DPU OS via the DPU BMC and places the DPU in the restricted mode. In this mode the host cannot ssh into the DPU or program the DPU network accelerators.

## DPU OS Installation
Use the ansible playbooks, [DPU NvCert Bootstrap](https://gitlab.com/nvidia/networking/bluefield/nvcert/dpu_nvcert_bootstrap), to install the DPU image and the relevant packages.

The playbooks can be run from the Test-server to bootstrap {host-1, dpu-1} and {host-2, dpu-2}. You cannot run the playbooks from the server hosting the dpu (host-1 or host-2). This is because the host is power-cycled as a part of the installation process.

## Passwordless access to the x86-hosts for nvidia user
The tests require passwordless ssh access to the x86-hosts and DPUs from the test server using the nvidia/ubuntu account. Please follow the steps below to enable it:

- Create an account on test-server and both servers under test (x86 hosts) with the username "nvidia"
  For the DPUs, "ubuntu" account should be created automatically through the bootstrap ansible playbooks

Once you have the account on the hosts you can setup passwordless access from the test server via the following commands -
- Generate SSH key on the test server
`nvidia@test-server:~# ssh-keygen -f ~/.ssh/id_rsa -N ""`

- Copy the key to each of the x86 hosts and the DPUs
`nvidia@test-server:~# ssh-copy-id -i ~/.ssh/id_rsa.pub nvidia@x86-host-1-oob-ip`
`nvidia@test-server:~# ssh-copy-id -i ~/.ssh/id_rsa.pub nvidia@x86-host-2-oob-ip`
`nvidia@test-server:~# ssh-copy-id -i ~/.ssh/id_rsa.pub ubuntu@dpu-1-oob-ip`
`nvidia@test-server:~# ssh-copy-id -i ~/.ssh/id_rsa.pub ubuntu@dpu-2-oob-ip`

- Verify passwordless ssh into the x86 hosts and DPUs
`nvidia@test-server:~# ssh nvidia@x86-host-1-oob-ip`
#exit from the ssh session to host-1
`nvidia@test-server:~# ssh nvidia@x86-host-2-oob-ip`
#exit from the ssh session to host-2
 `nvidia@test-server:~# ssh ubuntu@dpu-1-oob-ip`
#exit from the ssh session to dpu-1
 `nvidia@test-server:~# ssh ubuntu@dpu-2-oob-ip`
 #exit from the ssh session to dpu-2

Once done,  do the following on both x86 hosts:
- Add the following line to the sudoers file (/etc/sudoers) to make sure your non-root user can run the needed binaries without a password:
 `%sudo ALL=(ALL) ALL, NOPASSWD: /usr/bin/bash,/usr/sbin/ip,/opt/mellanox/iproute2/sbin/ip,/usr/bin/mlxprivhost,/usr/bin/mst,/usr/bin/systemctl,/usr/sbin/ethtool,/usr/sbin/set_irq_affinity_cpulist.sh,/usr/bin/tee,/usr/bin/numactl,/usr/bin/awk,/usr/bin/rm -f /tmp/*,/usr/bin/taskset,/usr/sbin/dmidecode`
Note that this line gives permission to the users in sudo group to run (only) the specified commands as sudo without password.


## Quick Start
On the Test-server
- Clone repo
`git clone --recurse-submodules https://gitlab.com/nvidia/networking/bluefield/nvcert/dpu_nvcert_tests.git`
`cd dpu_nvcert_tests`
- Use vim or nano to edit the hosts.yaml file and set remote access info for the nodes in the test topology
- Setup the packages needed for running the tests `sudo ./install_nvcert_utility_sw.py`
- Run tests as described in the `Test Usage` section

## DPU mode for running tests
These tests can be run on a DPU that is in `Zero Trust mode` or in  `Host Trusted mode` -

You can verify the mode by running the following command on the host -
```
root@x86-host:~# mlxprivhost -d /dev/mst/mt41692_pciconf0 q
Host configurations
-------------------
level                         : RESTRICTED

Port functions status:
-----------------------
disable_rshim                 : TRUE
disable_tracer                : TRUE
disable_port_owner            : TRUE
disable_counter_rd            : TRUE

root@x86-host:~#
```

## Remote access info for the test nodes
Edit the inventory file, hosts.yaml, and add access info to the x86-hosts and dpus.
For each of the x86-hosts (host_1 and host_2) add OOB address user and password.
```
    oob_address: <host IPv4 address>
    user: root
    password: <host-password>
```

For each of the x86-hosts specify the uplink port id for testing (this is a zero based number). The first port on the network card (port 0) is used by default.
```
    uplink_port: 0
```
### Zero-trust mode
In this mode the host cannot access the DPU OS or NIC config. The DPU OOB address needs to be specified for configuring the DPU.

For each of the dpus (dpu_1 and dpu_2) add oob-address, user and password.
```
    oob_address: <DPU IPv4 OOB address>
    user: <dpu-user-name>
    password: <dpu-password>
```

### Host Trusted mode
In this mode the x86-host can use the rshim network to ssh into the DPU and acquire the DPU OOB address.

If the x86-server is hosting multiple DPUs you need to specify the rshim-id associated with the DPU being tested. The rshim_id is only needed if you do not know the DPU OOB address and require the tests to use the host-rshim-network to fetch the DPU OOB address. If the server is hosting a single DPU the rshim_num can be left at the default value of `0`.
```
    rshim_num: 0
```

For each of the dpus (dpu_1 and dpu_2) add oob-address, user and password. If the DPU OOB address is unknown it can be specified as 0.0.0.0, in which case the test will use the host-rshim network to lookup the DPU oob_net0 inet address.
```
    oob_address: 0.0.0.0
    user: <dpu-user-name>
    password: <dpu-password>
```

### Inventory file
hosts.yaml is the default inventory file. If you prefer to not edit hosts.yaml and instead have a separate inventory file you can specify that via the -i option. For e.g. `./dpu-tests -i inventory-1.yaml tcp-ovs`

## Test Usage
### Workflow
<img src=./docs/dpu_nvcert_tests_flow.png alt="workflow" width=50% height=50% title="workflow">

Use the [DPU Nvcert Bootstrap](https://gitlab.com/nvidia/networking/bluefield/nvcert/dpu_nvcert_bootstrap) ansible playbooks for steps 1, 5, 6, 8, 11. Each {host, DPU} needs to be bootstrapped separately.

### Secure Boot Test Usage
- The test checks if the server is able to run with Secure Boot
- Run the secure boot test in the dpu_nvcert_tests repo
`./dpu-tests secureboot`

### OVS Performance Test Usage
<img src=./docs/OVS-Topology.jpg alt="OVS" width=60% height=60% title="OVS">

[OVS Test Description](https://github.com/Mellanox/ngc_multinode_perf/blob/main/README.md) - Run performance tests on the x86-hosts with the default, HW-accelerated OVS bridge on the DPU.
- Use the [DPU Nvcert Bootstrap](https://gitlab.com/nvidia/networking/bluefield/nvcert/dpu_nvcert_bootstrap) ansible playbooks to install DOCA on each {x86-host, dpu} with `HBN disabled`
- Run the TCP performance tests in the dpu_nvcert_tests repo
`./dpu-tests tcp-ovs`
- Run the RDMA tests in the dpu_nvcert_tests repo
`./dpu-tests rdma-ovs`

### IPSec Performance Test Usage
- No additional installation steps needed if you already installed DOCA with `HBN disabled`
- Run the TCP performance tests in the dpu_nvcert_tests repo
`./dpu-tests tcp-ipsec`

### HBN EVPN Performance Test Usage
<img src=./docs/HBN-Topology.jpg alt="HBN" width=60% height=60% title="HBN">

[HBN Test description](https://gitlab.com/nvidia/networking/bluefield/nvcert/dpu_nvcert_tests/-/blob/main/hbn/README.md) - This is a performance test on the x86-hosts with a) BGP-EVPN offloaded on the DPU Arm and b) VxLAN packet processing pipeline HW-accelerated on the DPU eSwitch.

- Use the [DPU NvCert Bootstrap](https://gitlab.com/nvidia/networking/bluefield/nvcert/dpu_nvcert_bootstrap) ansible playbooks to install DOCA on each {x86-host, dpu} pair with `-e hbn_enable=true`
- Start the HBN container on the DPUs using `ansible-playbook dpu-nvcert-install-hbn-container.yml` in the dpu_nvcert_bootstrap repo
- Run the TCP performance tests in the dpu_nvcert_tests repo with EVPN accelerated on the DPU
`./dpu-tests tcp-hbn-evpn`
- Run the RDMA performance tests in the dpu_nvcert_tests repo with EVPN accelerated on the DPU
`./dpu-tests rdma-hbn-evpn`

### Running tests NIC Mode
 `./dpu-tests tcp-ovs`
 `./dpu-tests rdma-ovs`
- Use the [DPU Nvcert Bootstrap](https://gitlab.com/nvidia/networking/bluefield/nvcert/dpu_nvcert_bootstrap) ansible playbook `dpu-nvcert-config-nic-mode.yml` to configure the DPUs to be in NIC mode
- Run the TCP and RDMA tests (the only tests supported for NIC mode)

### Get setups Info
 `./dpu-tests system-info`
An option to gather system information at the end of the run of each nvcert test. This command creates a file [./system_info.txt] that has the collected information about the following: 2 hosts, 2 DPUs and for each DPU,
the ports info

## Additional documentation
- Use `./dpu-tests help` for command menu
- Use `./dpu-tests ping-<dpu-config>` for sanity checking the test setup
- [HBN Test Details](https://gitlab.com/nvidia/networking/bluefield/nvcert/dpu_nvcert_tests/-/blob/main/hbn/README.md)
- [OVS, IPSec Details](https://github.com/Mellanox/ngc_multinode_perf/blob/main/README.md)
- For Nvidia internal users:
    - [TCP Tuning](https://confluence.nvidia.com/pages/viewpage.action?spaceKey=NSWX&title=Tuning+Guide)
    - [Intel SPR Tuning for Best TCP Performance](https://confluence.nvidia.com/display/SW/Intel+SPR+Tuning+for+Best+TCP+performance)
    - [Intel Sapphire Rapid Can't handle PCIe Relaxed Ordering](https://confluence.nvidia.com/display/SW/Intel+Sapphire+Rapid+Can%27t+handle+PCIe+Relaxed+Ordering)

## Expected results
Numbers observed are with BF3-B3220, 200Gbps. TCP tests are unidirectional, RDMA tests are bidirectional.
|DPU-Config| TCP     | RDMA   |
|----------|---------|--------|
|OVS       | 199Gbps | 393Gbps|
|          | 99%     | 98%    |
|HBN-EVPN  | 198Gbps | 388Gbps|
|          | 99%     | 97%    |
|IPSec     | 198Gbps | -      |
|          | 99%     | -      |

## Rshim network management on a server hosting multiple DPUs
Host Rshim network is only available in the `Host Trusted` mode where the DPU is in default, ECPF/Privileged mode.
<img src=./docs/rshim-network.jpg alt="rshim" width=80% height=80% title="rshim">

### Authors
Hala Awisat <hawisat@nvidia.com>
Anuradha Karuppiah <anuradhak@nvidia.com>
