#!/usr/bin/env python3

##
## Copyright © 2023 NVIDIA CORPORATION & AFFILIATES. ALL RIGHTS RESERVED.
##
## This software product is a proprietary product of Nvidia Corporation and its affiliates
## (the "Company") and all right, title, and interest in and to the software
## product, including all associated intellectual property rights, are and
## shall remain exclusively with the Company.
##
## This software product is governed by the End User License Agreement
## provided with the software product.
##
## Author: Anuradha Karuppiah <anuradhak@nvidia.com>

# pylint: disable=too-many-instance-attributes

"""
Wrapper for various dpu tests -
1. Parses the remote access info for the hosts and DPU for hosts.yaml
2. Enables the netdev on the host, configure ip address and validates
   network connection
3. Runs the requested benchmark test; type dpu_test for usage
"""
import sys
import get_info
sys.path.insert(0, 'hbn')
try:
    import argparse
    import ipaddress
    import logging
    import os
    import re
    import subprocess
    import time
    import hbn_evpn
    from utils import node
    from utils import remote_access_setup
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


logger = logging.getLogger("dpu_nvcert")


class TestServer(node.LocalNode):
    """
    Server from which the tests are being running. This server is expected
    to have passwordless ssh root access to the hosts and DPUs in the test
    topology.
    """
    # pass rate as a percentage of the port bandwidth
    _HBN_PASS_THROUGHPUT = 90
    _OVS_PASS_THROUGHPUT = 90
    _IPSEC_PASS_THROUGHPUT = 90

    def __init__(self, host_1, host_2, dpu_data = None):
        self.benchmark_tcp_test = "ngc_multinode_perf/ngc_tcp_test.sh"
        self.benchmark_rdma_test = "ngc_multinode_perf/ngc_rdma_test.sh"
        self.benchmark_ipsec_test = "ngc_multinode_perf/ngc_ipsec_full_offload_tcp_test.sh"
        self.benchmark_ipsec_setup = "ngc_multinode_perf/ipsec_full_offload_setup.sh"
        self.host_1 = host_1
        self.host_2 = host_2
        self.num_dpus = 0
        if dpu_data != None:
            self.num_dpus = 2
        self.dpu_data = dpu_data
        self.vteps = []
        self.dpus = []
        self.hbn_pass_rate = 0
        self.ovs_pass_rate = 0
        self.ipsec_pass_rate = 0
        self.compute_pass_rates()


    def compute_pass_rates(self):
        """
        set pass rates for the various benchmark tests
        """
        hosts = [self.host_1, self.host_2]
        speeds = [host.netdev.speed for host in hosts]
        # Interface speed is in Mbps; convert to Gbps to match with throughput
        min_host_rate = min(speeds)/1000
        self.hbn_pass_rate = (self._HBN_PASS_THROUGHPUT * min_host_rate)/100
        self.ovs_pass_rate = (self._OVS_PASS_THROUGHPUT * min_host_rate)/100
        self.ipsec_pass_rate = (self._IPSEC_PASS_THROUGHPUT * min_host_rate)/100


    def run_ping(self):
        """
        ping host-2 from host-1
        """
        count = 3
        sender = self.host_1.netdev.ipv4_addrs[0].ip
        receiver = self.host_2.netdev.ipv4_addrs[0].ip
        cmd = f"ping -c {count} -I {sender} {receiver}"
        output = self.host_1.sudo(cmd)
        ping_pat = re.compile(r"(?P<sent>\d+) packets transmitted, (?P<received>\d+) received")
        obj = ping_pat.search(output)
        if obj:
            sent = int(obj.group("sent"))
            received = int(obj.group("received"))
        else:
            sent = 0
            received = 0

        return bool(sent == count and received == count)


    def run_ping_with_backoff(self, backoff = 3, attempts = 3):
        """
        run a ping between the hosts. if it doesn't succeed, back off for 1 second
        and retry
        """
        r_val = False
        while attempts:
            r_val = self.run_ping()
            if r_val:
                break
            attempts = attempts - 1
            if attempts and backoff:
                time.sleep(backoff)

        return r_val


    def run_tcp_benchmark(self):
        """
        run iperf between host-1 and host-2 and measure network throughput
        """
        cmd = f"{self.benchmark_tcp_test}\
                {self.host_1.uname}@{self.host_1.oob_address}\
                {self.host_1.mlxdev}\
                {self.host_2.uname}@{self.host_2.oob_address}\
                {self.host_2.mlxdev} HALF CHANGE {CMDARGS.duration}"
        r_obj = self.run(cmd, warn = True, hide = "stderr", pty=True)
        # get throughput from r_obj
        # Sample output "Throughput is: 32.97 Gb/s"
        throughput_pat = re.compile(r"Throughput is: (?P<throughput>\S+) Gb/s")
        obj = throughput_pat.search(r_obj)
        throughput = float(obj.group("throughput")) if obj else 0

        return throughput

    def _create_vteps(self):
        """
        Setup EVPN configuration on the HBN containers running on each of the
        DPUs
        """
        for dpu_id in range(self.num_dpus):
            idx = dpu_id + 1
            name = f"dpu_{idx}"
            dpu_data = self.dpu_data[name]
            oob_address = dpu_data.get("oob_address")
            uname = dpu_data.get("user")
            password = dpu_data.get("password")
            uplink_port = dpu_data.get("uplink_port")
            vtep = hbn_evpn.Vtep(idx=idx, name=name,
                    oob_address=oob_address,
                    uname=uname, password=password,
                    uplink_port=uplink_port)
            if vtep.setup():
                logger.info("VTEP %s is ready", oob_address)
            else:
                logger.info("VTEP %s is not ready", oob_address)
                sys.exit("VTEP setup failed")

            self.vteps.append(vtep)


    def _create_dpus(self, set_mtu_jumbo=True):
        """
        Instantiate DPUs if commands need to run on them
        """
        for dpu_id in range(self.num_dpus):
            idx = dpu_id + 1
            name = f"dpu_{idx}"
            dpu_data = self.dpu_data[name]
            oob_address = dpu_data.get("oob_address")
            uname = dpu_data.get("user")
            password = dpu_data.get("password")
            dpu = node.DpuNode(oob_address=oob_address,
                    uname=uname, password=password)
            if set_mtu_jumbo:
                dpu.set_jumbo_mtu_on_uplinks()
            self.dpus.append(dpu)


    def get_host_macs(self):
        """
        get the hwaddresses of the hosts
        """
        macs = []
        macs.append(self.host_1.netdev.hwaddress)
        macs.append(self.host_2.netdev.hwaddress)
        return macs


    def create_evpn_vxlan_bridge(self):
        """
        Setup EVPN VxLAN bridge on the DPUs, ping from one host to
        another and check if the MACs are advertised via EVPN
        """
        self._create_vteps()
        # check if the hosts can talk to each other over the EVPN-VxLAN bridge
        if not self.run_ping_with_backoff():
            logger.info("Network connection is down between the hosts")
            return False
        logger.info("Network connectivity is up between the hosts")

        logger.debug("Sleeping for 10 seconds to accomodate inter-component delays")
        time.sleep(10)

        # check if the host macs (local and remote) are present in zebra
        macs = self.get_host_macs()
        for vtep in self.vteps:
            if not vtep.check_macs(macs):
                logger.info("EVPN mac table is missing the host macs %s", macs)
                return False

        logger.info("VTEP EVPN mac table has been populated")
        return True


    def run_evpn_tcp_benchmark(self):
        """
        Setup EVPN VxLAN bridge and run tcp performance benchmark over it
        """
        if not self.create_evpn_vxlan_bridge():
            return
        logger.info("Run TCP benchmark on the EVPN VxLAN Bridge")
        throughput = self.run_tcp_benchmark()
        if throughput > self.hbn_pass_rate:
            logger.info("EVPN benchmark tests passed with expected throughput %f Gbps", throughput)
        else:
            logger.info("EVPN tests failed; throughput %f Gbps less than expected", throughput)


    def run_ovs_tcp_benchmark(self):
        """
        Run tcp performance benchmark over the default ovs bridge
        """

        # Instantiate DPUs and set the uplink MTU to junbo
        # check if the hosts can talk to each other over the OVS bridge
        if not self.run_ping_with_backoff():
            logger.info("Network connection is down between the hosts")
            return
        logger.info("Network connection is up between the hosts")

        logger.info("Run TCP benchmark on the default hw-accelerated OVS Bridge")
        throughput = self.run_tcp_benchmark()
        if throughput > self.ovs_pass_rate:
            logger.info("OVS/TCP benchmark tests passed with expected throughput %f Gbps", throughput)
        else:
            logger.info("OVS/TCP tests failed; throughput %f Gbps less than expected", throughput)


    def run_rdma_benchmark(self):
        """
        Run rdma benchmark over whatever is configured on the DPU
        """

        logger.debug("Run RDMA benchmark")
        cmd = f"./{self.benchmark_rdma_test}\
                {self.host_1.oob_address}\
                {self.host_1.mlxdev}\
                {self.host_2.oob_address}\
                {self.host_2.mlxdev}"
        output = self.run(cmd, warn = True, hide = "stderr", pty = True)
        read_pat = re.compile(r"NGC ib_read_bw (?P<result>\S+)")
        obj = read_pat.search(output)
        if obj:
            logger.info("IB read %s", obj.group("result"))
        write_pat = re.compile(r"NGC ib_write_bw (?P<result>\S+)")
        obj = write_pat.search(output)
        if obj:
            logger.info("IB write %s", obj.group("result"))
        send_pat = re.compile(r"NGC ib_send_bw (?P<result>\S+)")
        obj = send_pat.search(output)
        if obj:
            logger.info("IB send %s", obj.group("result"))


    def run_ipsec_benchmark(self):
        """
        Run tcp performance benchmark over the default ovs bridge
        """

        logger.debug("Run IPSEC performance tests")

        dpu_data = self.dpu_data["dpu_1"]
        dpu_1_uname = dpu_data.get("user")
        dpu_1_oob_address = dpu_data.get("oob_address")
        dpu_data = self.dpu_data["dpu_2"]
        dpu_2_uname = dpu_data.get("user")
        dpu_2_oob_address = dpu_data.get("oob_address")
        # Instantiate DPUs to cache priv level
        self._create_dpus()

        # Check if ping works before configuring ipsec
        if not self.run_ping_with_backoff():
            logger.info("Network connection is down between the hosts before ipsec config")
            return

        # Setup the ipsec config
        cmd = f"{self.benchmark_ipsec_setup}\
                {self.host_1.uname}@{self.host_1.oob_address}\
                {self.host_1.mlxdev}\
                {self.host_2.uname}@{self.host_2.oob_address}\
                {self.host_2.mlxdev}\
                {dpu_1_uname}@{dpu_1_oob_address}\
                {dpu_2_uname}@{dpu_2_oob_address} 9500"
        self.run(cmd, warn = True, hide = "stderr", pty = True)

        # Check if ping works with ipsec
        if not self.run_ping_with_backoff():
            logger.info("Network connection is down between the hosts after ipsec config")
            return

        # Run TCP perf with ipsec
        throughput = self.run_tcp_benchmark()
        if throughput > self.ipsec_pass_rate:
            logger.info("IPSEC benchmark tests passed with expected throughput %f Gbps", throughput)
        else:
            logger.info("IPSEC tests failed; throughput %f Gbps less than expected", throughput)


    def run_secureboot_benchmark(self):
        """
        Run secure boot benchmark over whatever is configured on the DPU
        """
        logger.debug("Run Secure Boot benchmark")

        # Instantiate DPUs to cache priv level
        self._create_dpus()
        cmd = f"mokutil --sb-state"

        for dpu in self.dpus:
            output = dpu.sudo(cmd, warn = True, hide = "stderr", pty = True)
            output_pat = re.compile(r"SecureBoot (?P<result>\S+)")
            obj = output_pat.search(output)
            if obj:
                logger.info("SecureBoot %s", obj.group("result"))
                if "enabled" in obj.group("result"):
                    logger.info("SecureBoot test passed")
            else:
               logger.info("SecureBoot test failed")



def cleanup(all_dpu_data):
    """
    Revert DPU to privileged state
    """

    logger.debug("Return the DPU to the default ECPF mode")
    dpu_data = all_dpu_data["dpu_1"]
    dpu_1_uname = dpu_data.get("user")
    dpu_1_password = dpu_data.get("password")
    dpu_1_oob_address = dpu_data.get("oob_address")
    dpu_data = all_dpu_data["dpu_2"]
    dpu_2_uname = dpu_data.get("user")
    dpu_2_oob_address = dpu_data.get("oob_address")
    dpu_2_password = dpu_data.get("password")

    dpu_1 = node.DpuNode(oob_address=dpu_1_oob_address,
            uname=dpu_1_uname, password=dpu_1_password)
    dpu_2 = node.DpuNode(oob_address=dpu_2_oob_address,
            uname=dpu_2_uname, password=dpu_2_password)


def setup_host_ssh_keys():
    """
    Setup ssh keys on the test-server and copy to the x86-host
    """
    (host_1, host_2, _) = test_setup_resources()
    home_dir = os.path.expanduser("~")
    rsa_file = home_dir + "/.ssh/id_rsa"
    rsa_pub_file = home_dir + "/.ssh/id_rsa.pub"
    if not os.path.isfile(rsa_file) or not os.path.isfile(rsa_pub_file):
        cmd = f"ssh-keygen -f {rsa_file} -N ''"
        os.system(cmd)

    # if new keys were generated copy to the hosts
    cmd = f"sshpass -p {host_1.password} ssh-copy-id\
          -o StrictHostKeyChecking=no\
          -i {rsa_pub_file} {host_1.uname}@{host_1.oob_address}"
    os.system(cmd)
    cmd = f"sshpass -p {host_2.password} ssh-copy-id\
          -o StrictHostKeyChecking=no\
          -i {rsa_pub_file} {host_2.uname}@{host_2.oob_address}"
    os.system(cmd)


def setup_dpu_ssh_keys(host_1, host_2, orig_dpu_data):
    """
    IPSEC tests require passwordless access to the DPUs. Setup ssh keys on the
    test-server and copy to the DPU
    """
    home_dir = os.path.expanduser("~")
    rsa_file = home_dir + "/.ssh/id_rsa"
    rsa_pub_file = home_dir + "/.ssh/id_rsa.pub"
    new_keys = False
    if not os.path.isfile(rsa_file) or not os.path.isfile(rsa_pub_file):
        cmd = f"ssh-keygen -f {rsa_file} -N ''"
        os.system(cmd)
        new_keys = True

    # copy the key to the DPUs
    for dpu_id in range(2):
        idx = dpu_id + 1
        name = f"dpu_{idx}"
        dpu_data = orig_dpu_data[name]
        oob_address = dpu_data.get("oob_address")
        uname = dpu_data.get("user")
        password = dpu_data.get("password")

        cmd = f"sshpass -p {password} ssh-copy-id\
              -o StrictHostKeyChecking=no\
              -i {rsa_pub_file} {uname}@{oob_address}"
        print("%s", cmd)
        os.system(cmd)

    if new_keys:
        # if new keys were generated also copy to the hosts
        cmd = f"sshpass -p {host_1.password} ssh-copy-id\
              -o StrictHostKeyChecking=no\
              -i {rsa_pub_file} {host_1.uname}@{host_1.oob_address}"
        os.system(cmd)
        cmd = f"sshpass -p {host_2.password} ssh-copy-id\
              -o StrictHostKeyChecking=no\
              -i {rsa_pub_file} {host_2.uname}@{host_2.oob_address}"
        os.system(cmd)


def test_setup_add_host(idx, oob_address, host_data):
    """
    Instantiate a x86-host
    """
    uname = host_data.get("user")
    password = host_data.get("password")
    uplink_network = host_data.get("uplink_network", None)
    dev = host_data.get("mlx_dev")
    host = node.HostNode(idx=idx, oob_address=oob_address,
            dev=dev, uname=uname, password=password,
            uplink_network=uplink_network)

    return host


def test_validate_repo():
    """
    Check if the repo has been cloned correctly and if the patches have been
    applied
    """
    test_dir = "ngc_multinode_perf"
    num_files = len(os.listdir(test_dir))
    if not num_files:
        print("Sub-modules were not setup correctly")
        print("Please follow the instructions in the README.md to clone the repo.")
        sys.exit(-1)

    '''
    cwd = os.getcwd()
    os.chdir(test_dir)
    cmd = "git log --pretty=oneline origin/main..HEAD"
    patches = subprocess.check_output(cmd.split())
    if not patches:
        print("./install_nvcert_utility_sw.py was not run.")
        print("Please follow the instructions in the README.md to run the tests.")
        sys.exit(-1)

    os.chdir(cwd)
    '''


def test_setup_resources(add_hosts=True):
    """
    Set up the host resources needed for running commands remotely
    via paramiko
    """
    # Check if the test repo was setup correctly
    test_validate_repo()

    (data, nic_mode_1, nic_mode_2) = remote_access_setup.validate_test_setup(CMDARGS.inventory)

    # Parse node access info provided in hosts.yaml
    x86_hosts_data = data["x86-hosts"]
    host_1_data = x86_hosts_data["host_1"]
    host_2_data = x86_hosts_data["host_2"]

    host_addr_1 = ipaddress.IPv4Address(host_1_data["oob_address"])
    host_addr_2 = ipaddress.IPv4Address(host_2_data["oob_address"])

    # setup hosts
    if add_hosts:
        host_1 = test_setup_add_host(1, host_addr_1, host_1_data)
        host_2 = test_setup_add_host(2, host_addr_2, host_2_data)
        logger.debug("%s %s", host_1, host_2)
    else:
        host_1 = host_2 = None

    dpu_data = data["dpus"]
    dpu_1 = dpu_2 = None

    if not nic_mode_1:
        dpu_1_data = dpu_data["dpu_1"]
        dpu_1 = ipaddress.IPv4Address(dpu_1_data["oob_address"])
    if not nic_mode_2:
        dpu_2_data = dpu_data["dpu_2"]
        dpu_2 = ipaddress.IPv4Address(dpu_2_data["oob_address"])

    logger.info("host-1: %s host-2: %s dpu-1: %s dpu-2: %s",
                host_addr_1, host_addr_2, dpu_1, dpu_2)

    return (host_1, host_2, dpu_data)


def test_rdma_hbn_evpn_bridge():
    """
    Setup an EVPN VxLAN bridge and run RoCE performance tests
    """
    logger.info("Beginning execution of DPU NvCert HBN RDMA Tests")

    (host_1, host_2, dpu_data) = test_setup_resources()
    # bootstrap the test server with the hosts info and run benchmark tests
    test_server = TestServer(host_1, host_2, dpu_data)

    # Setup an EVPN VxLAN bridge
    if not test_server.create_evpn_vxlan_bridge():
        return

    test_server.run_rdma_benchmark()


def test_tcp_hbn_evpn_bridge():
    """
    Setup an EVPN VxLAN bridge and run TCP performance tests
    """
    logger.info("Beginning execution of DPU NvCert HBN Tests")

    (host_1, host_2, dpu_data) = test_setup_resources()
    # bootstrap the test server with the hosts info and run benchmark tests
    test_server = TestServer(host_1, host_2, dpu_data)
    test_server.run_evpn_tcp_benchmark()


def test_tcp_ovs_bridge():
    """
    Run TCP performance tests over the default OVS bridges
    """
    logger.info("Beginning execution of DPU NvCert OVS Tests")

    (host_1, host_2, dpu_data) = test_setup_resources()
    # bootstrap the test server with the hosts info and run benchmark tests
    test_server = TestServer(host_1, host_2, dpu_data)
    test_server.run_ovs_tcp_benchmark()


def test_rdma_ovs():
    """
    Run RDMA performance tests
    """
    logger.info("Beginning execution of DPU NvCert RDMA Tests")

    (host_1, host_2, dpu_data) = test_setup_resources()
    # bootstrap the test server with the hosts info and run benchmark tests
    test_server = TestServer(host_1, host_2, dpu_data)

    # Check if ping works
    if not test_server.run_ping_with_backoff():
        logger.info("Network connection is down between the hosts")
        return

    test_server.run_rdma_benchmark()


def test_not_ready():
    """
    Common function to skip out on tests that are not ready
    """
    logger.info("Sorry, test is not ready for use")


def test_tcp_ipsec():
    """
    Run IPSEC performance tests
    """
    logger.info("Beginning execution of DPU NvCert IPSEC Tests")

    (host_1, host_2, dpu_data) = test_setup_resources()
    # ipsec tests requires passwordless access to the DPUs, setup the
    # ssh keys
    setup_dpu_ssh_keys(host_1, host_2, dpu_data)
    # bootstrap the test server with the hosts info and run benchmark tests
    test_server = TestServer(host_1, host_2, dpu_data)
    test_server.run_ipsec_benchmark()


def test_ping_hbn_evpn():
    """
    Setup an EVPN VxLAN bridge and run ping
    """
    logger.info("Beginning execution of DPU NvCert EVPN ping")

    (host_1, host_2, dpu_data) = test_setup_resources()
    # bootstrap the test server with the hosts info and run benchmark tests
    test_server = TestServer(host_1, host_2, dpu_data)

    # Setup EVPN configuration
    test_server.create_evpn_vxlan_bridge()

    if test_server.run_ping_with_backoff():
        logger.info("Test Setup is healthy")
    else:
        logger.info("Network connection is down between the hosts")
        logger.info("Test Setup is rotten")


def test_secure_boot():
    """
    Run Secure Boot test
    """
    logger.info("Beginning execution of DPU NvCert Secure boot test")

    (host_1, host_2, dpu_data) = test_setup_resources()
    # bootstrap the test server with the hosts info and run benchmark tests
    test_server = TestServer(host_1, host_2, dpu_data)
    test_server.run_secureboot_benchmark()


def test_ping_ovs():
    """
    Run ping over whatever may already be configured on the DPUs
    """
    logger.info("Beginning execution of DPU NvCert Ping Tests")

    (host_1, host_2, dpu_data) = test_setup_resources()
    # bootstrap the test server with the hosts info and run benchmark tests
    test_server = TestServer(host_1, host_2, dpu_data)
    if test_server.run_ping_with_backoff():
        logger.info("Test Setup is healthy")
    else:
        logger.info("Network connection is down between the hosts")
        logger.info("Test Setup is rotten")


def reset_to_default():
    """
    Restore DPU to privileged mode
    """
    logger.info("Reset DPU to default settings")

    (_, _, dpu_data) = test_setup_resources(add_hosts=False)
    cleanup(dpu_data)

    logger.info("Please reboot the x86-host to enforce defaults")


def gather_system_info():
    if os.path.exists('./system_info.txt'):
        os.remove('./system_info.txt')
    (host_1, host_2, dpu_data) = test_setup_resources()
    print("******************* Host 1 *******************")
    get_info.print_info_from_host(host_1.oob_address, host_1)
    print("******************* Host 2 *******************")
    get_info.print_info_from_host(host_2.oob_address, host_2)
    print("******************* DPU 1 *******************")
    oob_address = dpu_data["dpu_1"].get("oob_address")
    uname = dpu_data["dpu_1"].get("user")
    password = dpu_data["dpu_1"].get("password")
    dpu_1 = node.DpuNode(oob_address=oob_address,
                    uname=uname, password=password)
    get_info.print_info_from_bf(dpu_1.oob_address, dpu_1)
    print("******************* DPU 2 *******************")
    oob_address = dpu_data["dpu_2"].get("oob_address")
    uname = dpu_data["dpu_2"].get("user")
    password = dpu_data["dpu_2"].get("password")
    dpu_2 = node.DpuNode(oob_address=oob_address,
                    uname=uname, password=password)
    get_info.print_info_from_bf(dpu_2.oob_address, dpu_2)


dpuNvcertCmds = {
    #Command            : ( Function, Help)
    "secureboot"          : (test_secure_boot , "Run Secure Boot test"),
    "rdma-ovs"          : ( test_rdma_ovs, "Run RDMA performance tests with OVS DPU config"),
    "rdma-hbn-evpn"     : ( test_rdma_hbn_evpn_bridge,
            "Run TCP performance tests with EVPN VxLAN DPU config"),
    "tcp-hbn-evpn"      : ( test_tcp_hbn_evpn_bridge,
            "Run TCP performance tests with EVPN VxLAN DPU config"),
    "tcp-ovs"           : ( test_tcp_ovs_bridge,
            "Run TCP performance tests with OVS DPU config"),
    "tcp-ipsec"         : ( test_tcp_ipsec,
            "Run TCP performance tests with IPSEC DPU config"),
    "ping-ovs"          : ( test_ping_ovs, "Run ping with OVS DPU config"),
    "ping-hbn-evpn"     : ( test_ping_hbn_evpn, "Run ping with EVPN VxLAN DPU config"),
    "cleanup"           : ( reset_to_default, "Reset the DPU to default settings"),
    "setup-ssh-key"     : ( setup_host_ssh_keys, "Setup SSH key on the x86-hosts"),
    "system-info"       : ( gather_system_info, "Gather system information"),
    "hbn-evpn-bridge"   : ( test_tcp_hbn_evpn_bridge, "OBSOLETE, use tcp-hbn-evpn"),
    "ovs-bridge"        : ( test_tcp_ovs_bridge, "OBSOLETE, use tcp-ovs"),
    "rdma"              : ( test_rdma_ovs, "OBSOLETE, use rdma-ovs"),
}


def parse_cmd_line():
    """
    Parse the command line options using the standard argparse library.
    """
    # The default values for the arguments
    # switch this to WARNING (fixme)
    default_log_level  = "DEBUG"

    epilog = "Usage dpu_tests [command]:\n"
    for cmd in sorted(dpuNvcertCmds.keys()):
        if "OBSOLETE" in dpuNvcertCmds[cmd][1]:
            # If the command is OBSOLETE it has been retaine for backwards
            # compatability; don't display it in the menu
            continue
        epilog += f"{cmd: <20} {dpuNvcertCmds[cmd][1]}\n"

    parser = argparse.ArgumentParser(description="DPU NvCert tests CLI",
            epilog=epilog,
            formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("command", choices=list(dpuNvcertCmds.keys()),
                        nargs="?", metavar="command", help="command to execute")
    parser.add_argument("--loglevel", "-l", default=default_log_level,
                        help="The logging level for output")
    parser.add_argument("--log-stdout", default=True, action="store_true",
                        help="Set logging to stdout")
    parser.add_argument("--inventory", "-i", default="hosts.yaml",
                        help="Specify the inventory yaml file, default is hosts.yaml")
    parser.add_argument("--duration", "-d", default=120,
                        help="Specify the test-traffic duration in seconds")
    args = parser.parse_args()
    return (args, epilog)


def init_logging():
    """
    Set up the logging according to the command line parameters provided.
    """
    log_cfg = CMDARGS.loglevel
    if CMDARGS.log_stdout:
        handler = logging.StreamHandler(sys.stdout)
    else:
        facility = logging.handlers.SysLogHandler.LOG_DAEMON
        handler = logging.handlers.SysLogHandler(address="/dev/log", facility=facility)
    formatter = logging.Formatter("nvcert[%(process)d]: %(message)s", None)
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    log_level = getattr(logging, log_cfg.upper(), None)
    if not isinstance(log_level, int):
        raise ValueError(f"Invalid log level: {CMDARGS.loglevel}")
    logger.setLevel(log_level)


def main():
    """
    Run all the hbn tests
    """
    global CMDARGS
    # Parse the command line parameters and initialize logging.
    (CMDARGS, epilog) = parse_cmd_line()

    init_logging()

    if not CMDARGS.command:
        print(epilog)
        sys.exit(-1)

    if CMDARGS.command in dpuNvcertCmds:
        dpuNvcertCmds[CMDARGS.command][0]()
    else:
        print(epilog)


if __name__ == "__main__":
    main()
