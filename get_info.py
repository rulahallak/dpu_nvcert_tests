#!/usr/bin/env python3
"""
****************************************************************************
Copyright (c) 2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.

This software product is a proprietary product of NVIDIA CORPORATION &
AFFILIATES (the "Company") and all right, title, and interest in and to the
software product, including all associated intellectual property rights, are
and shall remain exclusively with the Company.

This software product is governed by the End User License Agreement
provided with the software product.
****************************************************************************
File name:      get_info.py
Description:    Script to gather all information of the environment
Date:           September 2023
Python version: 3.8
Author:         Hala Awisat
****************************************************************************
"""


import re
import sys
import prettytable
import logging
from enum import Enum

sys.path.insert(0, 'hbn')
try:
    from utils import node
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc



# General
logger = logging.getLogger("dpu_nvcert")
ANSI_COLOR_REMOVE = re.compile(r"\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])")

# Constants
DEFAULT_COMMAND_TIMEOUT_IN_SEC = 30

# Classes
class OsType(Enum):
    UBUNTU = "Ubuntu"
    RHEL = "Red Hat"
    CENTOS = "CentOs"
    ORACLE = "Oracle"

    def __str__(self):
        return str(self.value)


class Port:
    def __init__(self):
        # Port info
        self.state = None
        self.physical_state = None
        self.speed = None
        self.width = None
        self.fec = None
        self.loopback_mode = None
        self.auto_negotiation = None

        # Cable info
        self.identifier = None
        self.compliance = None
        self.cable_technology = None
        self.cable_type = None
        self.oui = None
        self.vendor_name = None
        self.vendor_part_number = None
        self.vendor_serial_number = None
        self.rev = None
        self.wavelength = None
        self.transfer_distance = None
        self.attenuation = None
        self.fw_version = None
        self.digital_diagnostic_monitoring = None
        self.power_class = None
        self.cdr_rx = None
        self.cdr_tx = None
        self.los_alarm = None
        self.rx_power_current = None
        self.tx_power_current = None
        self.ib_cable_width = None
        self.cable_breakout = None
        self.nominal_bit_rate = None
        self.manufacturing_date = None

    def print_info(self, port_number=""):
        """
        Print Ports's info
        """
        port_table = prettytable.PrettyTable()
        port_table.header = False
        if port_number:
            port_table.title = f"Port {port_number} info"
        else:
            port_table.title = f"Port info"

        for k, v in vars(self).items():
            k = k.split("_")
            k = [x.capitalize() for x in k]
            k = "-".join(k)
            k = k.replace("Cdr", "CDR")
            k = k.replace("rx", "RX")
            k = k.replace("Oui", "OUI")
            k = k.replace("Fw", "FW")
            k = k.replace("Ib", "IB")

            # Handle list types
            if type(v) is list:
                if not v:
                    port_table.add_row([k, v])
                else:
                    port_table.add_row([k, v[0]])
                    if len(v) > 1:
                        for item in v[1:]:
                            port_table.add_row(["", item])
            else:
                port_table.add_row([k, v])
        port_table.align = "l"
        # print info
        message = f"\n{port_table}"
        with open('./system_info.txt', 'a+') as f:
            f.write(str(message))


class Platform:
    def __init__(self, ip, node):

        # General
        self.verbose = False
        self.debug = False
        self.server_name = None
        self.ip = ip
        self.os = None
        self.node = node

        # Platform Info
        self.os_version = None
        self.kernel = None
        self.cpu_architecture = None
        self.cpu_vendor = None
        self.python = None
        self.python3 = None
        self.dpdk = None
        self.rxp_compiler = None
        self.mft_version = None
        self.ofed_version = None

        # DOCA info
        self.doca_apps = None
        self.doca_apps_dev = None
        self.doca_grpc = None
        self.doca_grpc_dev = None
        self.doca_libs = None
        self.doca_libs_dev = None
        self.doca_prime_runtime = None
        self.doca_prime_sdk = None
        self.doca_prime_tools = None
        self.doca_runtime = None
        self.doca_samples = None
        self.doca_sdk = None
        self.doca_tools = None

    def execute_cmd(self,cmd: str):
        """
        Execute linux commands on platform

        Args:
            cmd: linux command in Ubuntu. In text format

        Return:
            output of the command
        """
        if "sudo" in cmd:
            result = self.node.sudo(cmd)
        else:
            result = self.node.run(cmd)
        return result.stdout

    def set_attr_by_cmd(self, field, cmd):
        """
        Execute linux command on platform and set the attr according the result

        Args:
            field: name of the desired field
            cmd: linux command in Ubuntu. In text format

        Return:
            None
        """
        try:
            result = self.execute_cmd(cmd)

        except Exception as exc:
            logger.warning(f"get {field} cmd exec failed: {exc}")
            return None

        if not result:
            logger.warning(f"{field} is missing")
            return None

        setattr(self, field, result.strip())

    def get_os(self):
        """
        Get the os of the platform.
        If not collected before - collect it.

        Return:
            The os of the platform.
        """
        if self.os:
            return self.os.value

        cmd = ("cat /etc/os-release | grep 'PRETTY_NAME=' -m 1 | cut -d= -f2 | tr -d '\"'")
        result = self.execute_cmd(cmd)

        if not result:
            logger.error(f"OS not found")
            raise Exception(f"OS not found")

        self.os_version = result.strip()

        os_name_out = result.lower().strip()
        if "ubuntu" in os_name_out:
            self.os = OsType.UBUNTU
        elif "red hat" in os_name_out:
            self.os = OsType.RHEL
        elif "centos" in os_name_out:
            self.os = OsType.CENTOS
        elif "oracle" in os_name_out:
            self.os = OsType.ORACLE
        else:
            logger.error(f"OS {os_name_out} not supported")
            raise Exception(f"OS {os_name_out} not supported")
        return self.os

    def get_kernel(self):
        """
        Get the Kernel version of the platform

        Return:
            The Kernel version of the platform.
        """
        if self.kernel:
            return self.kernel

        cmd = "uname -r"
        self.set_attr_by_cmd("kernel", cmd)
        return self.kernel

    def get_cpu_architecture(self):
        """
        Get the CPU architecture from the platform

        Return:
            The CPU architecture of the platform.
        """

        cmd = 'lscpu | grep "Architecture:" | cut -d":" -f2'
        self.set_attr_by_cmd("cpu_architecture", cmd)
        return self.cpu_architecture

    def get_cpu_vendor(self):
        """
        Get the CPU vendor from the platform

        Return:
            The CPU vendor of the platform.
        """

        cmd = 'lscpu | grep "Vendor ID:" | head -1 | cut -d":" -f2'
        self.set_attr_by_cmd("cpu_vendor", cmd)
        return self.cpu_vendor

    def get_python_version(self):
        """
        Gets python/python3 versions of  the platform

        Return:
            python/python3 versions of platform
        """
        if not self.python:
            cmd = "python -V | awk '{print $NF}' 2>&1"
            self.set_attr_by_cmd("python", cmd)

        if not self.python3:
            cmd = "python3 -V | awk '{print $NF}' 2>&1"
            self.set_attr_by_cmd("python3", cmd)

        return self.python, self.python3

    def get_mft_version(self):
        """
        Get the mft version of the device

        Return:
            mft version
        """
        if self.mft_version:
            return self.mft_version

        cmd = 'mst version | cut -d" " -f3 | tr -d "," '
        self.set_attr_by_cmd("mft_version", cmd)
        return self.mft_version

    def get_ofed_version(self):
        """
        Get the OFED version of the device

        Return:
            OFED version
        """
        if self.ofed_version:
            return self.ofed_version
        cmd = 'ofed_info -s | cut -d"-" -f 2,3 | cut -d":" -f1'
        self.set_attr_by_cmd("ofed_version", cmd)
        return self.ofed_version

    def get_server_name(self):
        """
        Get the server's name

        Return:
            Server's name
        """
        if self.server_name:
            return self.server_name

        cmd = "hostname"
        self.set_attr_by_cmd("server_name", cmd)
        return self.server_name


class Host(Platform):
    def __init__(self, ip, host_obj):
        super().__init__(ip, host_obj)

        # DOCA
        self.doca_host_repo = None

        # Desktop Management Interface info
        self.manufacturer = None
        self.product_name = None
        self.version = None
        self.serial_number = None
        self.uuid = None
        self.wake_up_type = None
        self.sku_number = None
        self.family = None

    def collect_info(self):
        """
        Get the info from the device
        """
        self.get_ip()
        self.get_server_name()
        self.get_os()
        self.get_ofed_version()
        self.get_kernel()
        self.get_cpu_architecture()
        self.get_cpu_vendor()
        self.get_python_version()
        self.get_dpdk()
        self.get_rxp_compiler()
        self.get_doca()
        self.get_mft_version()
        self.get_dmidecode_info()

    def print_info(self):
        """
        Print host's info
        """
        host_table = prettytable.PrettyTable()
        host_table.header = False
        host_table.title = "Host info"
        for k, v in vars(self).items():
            if k == "verbose" or k == "debug" or k == "node":
                continue
            k = k.split("_")
            k = [x.capitalize() for x in k]
            k = "-".join(k)
            k = k.replace("Cpu", "CPU")
            k = k.replace("Ip", "IP")
            k = k.replace("Pf_vf", "PF\VF")
            k = k.replace("Os", "OS")

            # Handle list types
            if type(v) is list:
                if not v:
                    host_table.add_row([k, v])
                else:
                    host_table.add_row([k, v[0]])
                    if len(v) > 1:
                        for item in v[1:]:
                            host_table.add_row(["", item])
            # Handle dictionary types
            elif type(v) is dict:
                if not v:
                    continue
                for key, value in v.items():
                    host_table.add_row([key, value])
            else:
                host_table.add_row([k, v])
        host_table.align = "l"
        # print info
        message = f"\n{host_table}"
        with open('./system_info.txt', 'a+') as f:
            f.write(str(message))

    def get_ip(self):
        """
        Get IP address of the Host

        Return:
            the ip of the host
        """
        if self.ip:
            return self.ip

        else:
            raise Exception(f"No host IP supplied")

    def get_dpdk(self):
        """
        This function gets current DPDK version of Host

        Return:
            the host's DPDK version
        """
        cmd = 'dpkg --list | grep "mlnx-dpdk " | tr -s " " | cut -d " " -f3'
        self.set_attr_by_cmd("dpdk", cmd)
        return self.dpdk

    def get_rxp_compiler(self):
        """
        This function gets current RXP-Compiler version of Host

        Return:
            the host's RXP-Compiler version
        """
        cmd = 'dpkg --list | grep rxp-compiler | tr -s " " | cut -d " " -f3'
        self.set_attr_by_cmd("rxp_compiler", cmd)
        return self.rxp_compiler

    def get_doca(self):
        """
        This function gets current DOCA version of Host
        """
        cmd = "dpkg --list | grep --color=never doca"

        try:
            stdout = self.execute_cmd(cmd)
        except Exception as e:
            logger.info("doca info is missing.")
            return

        if stdout:
            if (
                self.os == OsType.RHEL
                or self.os == OsType.CENTOS
                or self.os == OsType.ORACLE
            ):
                for line in stdout.splitlines():
                    if not line or len(line.split()) < 2:
                        continue
                    val = line.split()[1]
                    field_name = line.split()[0].split(".")[0].replace("-", "_")
                    if field_name.endswith("el"):
                        field_name = field_name[:-2]
                    if "doca_host_repo" in field_name:
                        field_name = "doca_host_repo"
                    if field_name in vars(self).keys():
                        if val and ".el7" in val:
                            val = val[: val.find(".el7")]
                        setattr(self, field_name, val)

            elif self.os == OsType.UBUNTU:
                for line in stdout.splitlines():
                    if not line or len(line.split()) < 2:
                        continue
                    val = line.split()[2]
                    field_name = line.split()[1].replace("-", "_")
                    if field_name.endswith("el"):
                        field_name = field_name[:-2]
                    if "doca_host_repo" in field_name:
                        field_name = "doca_host_repo"
                    if "libdoca_libs_dev" in field_name:
                        field_name = "doca_libs_dev"
                    if field_name in dir(self):
                        if val and ".el7" in val:
                            val = val[: val.find(".el7")]
                        setattr(self, field_name, val)

        # Verify all doca related fields were found
        for k, v in vars(self).items():
            if not k.startswith("doca"):
                continue
            if v == None or v == "":
                logger.error(f"{k}_repo is missing")

    def get_pf_vf(self):
        """
        This function gets Physical Function and Virtual Function of Host

        Return:
            the pf_vf - only if it has already been collected before
        """
        if self.pf_vf:
            return self.pf_vf

        cmd = "ibdev2netdev"
        stdout = self.execute_cmd(self, cmd)
        if stdout:
            self.pf_vf = stdout.splitlines()

    def get_dmidecode_info(self):
        """
        This function gets dmidecode info (Desktop Management Interface)
        """
        cmd = "sudo dmidecode -t1"
        stdout = self.execute_cmd(cmd)
        if not stdout:
            return
        for line in stdout.splitlines():
            splited_line = line.split(":")
            if not line or len(splited_line) < 2:
                continue
            val = splited_line[1].strip()
            field_name = (
                splited_line[0].replace("-", "_").strip().replace(" ", "_").lower()
            )
            setattr(self, field_name, val)
        return


class BF(Platform):
    def __init__(self, ip, bf_obj, mlxvpd_info=None):
        super().__init__(ip, bf_obj)

        # General
        self.os = None
        self.mlxvpd_info = mlxvpd_info
        self.ports = {}
        self.mst_devices_list = []

        # BF Info
        self.core_frequency = None
        self.device_type = None
        self.part_number = None
        self.device_description = None
        self.pcie = None
        self.crypto_mode = None
        self.ddr = None
        self.oob = None
        self.psid = None
        self.fw = None
        self.pci_device_name = None
        self.base_mac = None
        self.operation_mode = None
        self.mlx_regex = None
        self.virtio_net_controller = None
        self.collectx_clxapi = None
        self.libvma = None
        self.libxlio = None
        self.dpcp = None
        self.bf_release = None
        self.uefi = None
        self.ofed_version = None
        self.ovs = None
        self.mlnx_libsnap = None
        self.mlnx_snap = None
        self.spdk = None
        self.rxp_compiler_dev = None
        self.rxp_bench = None
        self.bfb = None
        self.part_number = None
        self.revision = None
        self.v2 = None
        self.serial_number = None
        self.v3 = None
        self.va = None
        self.misc_info = None
        self.vu = None
        self.checksum_complement = None
        self.board_id = None

    def collect_info(self):
        """
        Collect system info from BF
        """
        self.get_os()
        self.get_ofed_version()
        self.get_mft_version()
        self.get_server_name()
        self.get_kernel()
        self.get_cpu_architecture()
        self.get_cpu_vendor()
        self.get_core_frequency()
        # self.get_mlx_fw_device()
        # self.get_operation_mode()
        self.get_bf_release()
        self.get_bfb_info()
        self.get_bfb()
        self.get_python_version()
        self.get_mac()
        self.get_mlxvpd_info()
        self.get_ports()

    def print_info(self):
        """
        Print BF's info
        """
        bf_table = prettytable.PrettyTable()
        bf_table.header = False
        bf_table.title = "BF info"
        for k, v in vars(self).items():
            if (
                k == "device_description"
                or k == "verbose"
                or k == "debug"
                or k == "ports"
                or k == "mst_devices_list"
                or k == "mlxvpd_info"
                or k == "node"
            ):
                continue
            k = k.split("_")
            k = [x.capitalize() for x in k]
            k = "-".join(k)
            k = k.replace("Cpu", "CPU")
            k = k.replace("Ip", "IP")
            k = k.replace("Pf_vf", "PF\VF")
            k = k.replace("Os", "OS")
            k = k.replace("Uefi", "UEFI")
            k = k.replace("Ovs", "OVS")
            k = k.replace("Ofed", "OFED")
            k = k.replace("Mft", "MFT")
            k = k.replace("Ddr", "DDR")
            k = k.replace("Oob", "OOB")
            k = k.replace("Pci", "PCI")
            k = k.replace("Psid", "PSID")
            k = k.replace("Bfb", "BFB")
            k = k.replace("Bf", "BF")
            k = k.replace("Va", "VA")
            k = k.replace("Vu", "VU")

            # Handle list types
            if type(v) is list:
                if not v:
                    bf_table.add_row([k, v])
                else:
                    bf_table.add_row([k, v[0]])
                    if len(v) > 1:
                        for item in v[1:]:
                            bf_table.add_row(["", item])
            else:
                bf_table.add_row([k, v])
        bf_table.align = "l"

        # print info
        message = f"\n{bf_table}"
        with open('./system_info.txt', 'a+') as f:
            f.write(str(message))
        for number, port in enumerate(self.ports.values(), start=1):
            port.print_info(number)

    def get_mst_devices_list(self):
        """
        This function gets a list of mst devices on the BF

        Return:
            A list of mst devices
        """
        if self.mst_devices_list:
            return self.mst_devices_list
        cmd = "sudo mst status -v"
        stdout = self.execute_cmd(cmd)
        if not stdout:
            logger.error(f"mst name list is missing")
            raise Exception(f"mst name list is missing")
        stdout.split("PCI devices:")[1].split("Cable devices:")[0].strip()
        for line in stdout.splitlines():
            if "BlueField" not in line:
                continue
            mst_name = line.split()[1].strip()
            self.mst_devices_list.append(mst_name)
        self.mst_devices_list.sort()
        return self.mst_devices_list

    def get_port_info(self, mst_name):
        """
        This function gets the info for a port

        Args:
            mst_name: The desired mst device for checking

        Returns:
            A port object with info.
        """
        port = Port()
        cmd = f"sudo mlxlink -d {mst_name} -m"
        stdout = self.execute_cmd(cmd)
        stdout = ANSI_COLOR_REMOVE.sub("", stdout)
        if not stdout:
            logger.error(f"Unable to get cable info")
            raise Exception(f"Unable to get cable info")

        # Operational Info
        operational_info = (
            stdout.split("Operational Info")[1].split("Supported Info")[0].strip()
        )
        for line in operational_info.splitlines():
            if not line or line.find(":") == -1:
                continue
            splitted_line = line.split(":")
            val = splitted_line[1].strip()
            field_name = splitted_line[0].strip().replace(" ", "_").lower()
            if field_name in vars(port).keys():
                setattr(port, field_name, val)

        # Get Cable info
        cable_info = stdout.split("Module Info")[1].strip()
        for line in cable_info.splitlines():
            if not line or line.find(":") == -1:
                continue
            splitted_line = line.split(":")
            val = splitted_line[1].strip()
            field_name = (
                splitted_line[0].split("[")[0].strip().replace(" ", "_").lower()
            )

            # Unique cases:
            if "attenuation" in field_name:
                field_name = "attenuation"

            if field_name in vars(port).keys():
                setattr(port, field_name, val)
        return port

    def get_ports(self):
        """
        This function finds and extracts the info for all ports
        """
        mst_devices = self.get_mst_devices_list()
        for device in mst_devices:
            port = self.get_port_info(device)
            self.ports[device] = port

    def get_core_frequency(self):
        """
        Gets the core frequency of BF
        """
        cmd = 'sudo bfcpu-freq | cut -d " " -f4'
        self.set_attr_by_cmd("core_frequency", cmd)

    def get_mlx_fw_device(self):
        """
        Gets Mellanox devices firmware of BF
        """
        cmd = "mlxfwmanager"
        stdout = self.execute_cmd(cmd)
        # if stdout:
        for line in stdout.splitlines():
            if not line or len(line.split()) < 2:
                continue
            if line.find(":") != -1:
                val = line.split(":")[1].strip()
                field_name = line.split(":")[0].strip().replace(" ", "_").lower()
            elif len(line.split()) > 3:
                continue
            else:
                val = line.split()[1].strip()
                field_name = line.split()[0].strip().replace(" ", "_").lower()
            if field_name in vars(self).keys():
                setattr(self, field_name, val)
            if "description" in field_name:
                self.device_description = val
                for phrase in val.split(";"):
                    if "PCIe" in phrase:
                        self.pcie = phrase.strip()
                    elif "Crypto" in phrase:
                        self.crypto_mode = phrase.strip()
                    elif "DDR" in phrase:
                        self.ddr = phrase.strip()
                    elif "OOB" in phrase:
                        self.oob = phrase.strip()
        # Check values
        if not self.psid:
            logger.error("pid is missing")
        if not self.device_type:
            logger.error("device_type is missing")
        if not self.part_number:
            logger.error("part_number is missing")
        if not self.pcie:
            logger.error("pcie is missing")
        if not self.crypto_mode:
            logger.error("crypto mode is missing")
        if not self.ddr:
            logger.error("ddr is missing")
        if not self.oob:
            logger.error("oob is missing")
        if not self.device_description:
            logger.error("description is missing")

    def get_mac(self):
        """
        Get the mac address of the BF

        Requirers:
            pci_device_name

        Return:
            mac address of the BF
        """
        if self.base_mac:
            return self.base_mac
        if not self.pci_device_name:
            logger.error("pci device name missing. Cannot extract MAC address")
        cmd = f'flint -d {self.pci_device_name} q | grep "Base MAC" | cut -d ":" -f2 |  tr -s " " | cut -d " " -f2'
        self.set_attr_by_cmd("base_mac", cmd)
        return self.base_mac

    def get_operation_mode(self):
        """
        Get the current Operation Mode of BF

        Return:
            current Operation Mode of BF
        """
        if self.operation_mode:
            return self.operation_mode

        cmd = 'mlxconfig -d /dev/mst/mt41686_pciconf0 q INTERNAL_CPU_MODEL | grep INTERNAL_CPU_MODEL | tr -s " " | cut -d " " -f3'
        self.set_attr_by_cmd("operation_mode", cmd)
        return self.operation_mode

    def get_bf_release(self):
        """
        Get the BF-release info of BF

        Return:
            BF-release info of BF

        """
        if self.bf_release:
            return self.bf_release
        cmd = 'dpkg --list | grep bf-release | tr -s " " | cut -d " " -f3'
        self.set_attr_by_cmd("bf_release", cmd)
        return self.bf_release

    def get_bfb(self):
        """
        Get the BFB version of BF

        Return:
            BFB version of BF
        """
        if self.bfb:
            return self.bfb

        cmd = "cat /etc/mlnx-release"
        self.set_attr_by_cmd("bfb", cmd)
        return self.bfb

    def get_bfb_info(self):
        """
        Gets the BFB of BF
        """
        cmd = "bfb-info"
        stdout = self.execute_cmd(cmd)
        if not stdout:
            logger.error("Cannot get bfb info")
            return
        versions = stdout.split("Versions:")[1].split("Firmware:")[0].strip()
        # Versions
        for line in versions.splitlines():
            if not line:
                continue
            field_name = line.split(":")[0].lower().strip()
            if field_name in vars(self).keys():
                val = line.split(":")[1].strip()
                setattr(self, field_name, val)

        # mlx_regex
        mlx_regex = stdout.split("mlnx-dpdk:")[1].split("SNAP3:")[0].strip()
        for line in mlx_regex.splitlines():
            if not line:
                continue
            field_name = line.split()[0].lower().strip().replace("-", "_")
            if field_name in vars(self).keys():
                val = line.split()[1].strip()
                setattr(self, field_name, val)

        # SNAP3 + DOCA
        snap3_doca = stdout.split("SNAP3:")[1].split("OFED:")[0].strip()
        for line in snap3_doca.splitlines():
            if not line or not line.startswith("-") or not line[2:]:
                continue
            line = line[2:]
            field_name = line.split()[0].lower().strip().replace("-", "_")
            # Unique cases
            if field_name.endswith("el"):
                field_name = field_name[: (-len("el"))]
            if field_name == "rxpbench":
                field_name = "rxp_bench"
            if "librxpcompiler_dev" == field_name:
                field_name = "rxp_compiler_dev"
            if field_name == "libdoca_libs_dev":
                field_name = "doca_libs_dev"
            if field_name in vars(self).keys():
                val = line.split()[1].strip()
                setattr(self, field_name, val)

        # OFED
        ofed = stdout.split("OFED:")[1].strip()
        for line in ofed.splitlines():
            if not line:
                continue

            field_name = line.split()[0].lower().strip().replace("-", "_")
            if "openvswitch-switch" in line:
                field_name = "ovs"
            if "dpdk" in line:
                field_name = "dpdk"
            if field_name in vars(self).keys():
                val = line.split()[1].strip()
                setattr(self, field_name, val)

    def get_mlxvpd_info(self):
        """
        This function gets mlxvpd_info for BF
        """
        if self.mlxvpd_info:
            mlxvpd_info_list = self.mlxvpd_info.splitlines()[4:]

            self.part_number = " ".join(mlxvpd_info_list[0].split()[3:])
            self.revision = " ".join(mlxvpd_info_list[1].split()[2:])
            self.v2 = " ".join(mlxvpd_info_list[2].split()[2:])
            self.serial_number = " ".join(mlxvpd_info_list[3].split()[3:])
            self.v3 = " ".join(mlxvpd_info_list[4].split()[2:])
            self.va = " ".join(mlxvpd_info_list[5].split()[2:])
            self.misc_info = " ".join(mlxvpd_info_list[6].split()[3:])
            self.vu = " ".join(mlxvpd_info_list[7].split()[2:])
            self.checksum_complement = " ".join(mlxvpd_info_list[8].split()[3:])
            self.board_id = " ".join(mlxvpd_info_list[9].split()[3:])


# Functions
def print_info_from_host(ip, host_obj):
    host = Host(ip, host_obj)
    host.collect_info()
    host.print_info()


def print_info_from_bf(ip, bf_obj, ssh_port=None, mlxvpd_info=None):
    bf = BF(ip, bf_obj)
    bf.collect_info()
    bf.print_info()

