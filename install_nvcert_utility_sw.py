#!/usr/bin/env python3

##
## Copyright © 2023 NVIDIA CORPORATION & AFFILIATES. ALL RIGHTS RESERVED.
##
## This software product is a proprietary product of Nvidia Corporation and its affiliates
## (the "Company") and all right, title, and interest in and to the software
## product, including all associated intellectual property rights, are and
## shall remain exclusively with the Company.
##
## This software product is governed by the End User License Agreement
## provided with the software product.
##
## Author: Anuradha Karuppiah <anuradhak@nvidia.com>

"""
This is a standalone script that installs tne required packages on the
test-server
"""

import os
try:
    import pip
except ImportError:
    os.system("apt update")
    os.system("apt -y install python3-pip")
    import pip


def pip_import_or_install(package):
    """
    If a package doesn't exist on the test-server install it
    """
    try:
        __import__(package)
    except ImportError:
        pip.main(['install', package])


def apt_install():
    """
    Install apt packages needed for nvcert on the test-server
    """
    os.system("apt update")
    os.system("apt -y install bc")
    os.system("apt -y install sshpass")
    os.system("apt -y install git")


def apply_patches():
    """
    Apply any private patches to the sub-modules in the repo
    """

    cwd = os.getcwd()
    test_dir = "ngc_multinode_perf"
    patch_dir = "../patches/"
    os.chdir(test_dir)
    email = "EMAIL=anuradhak@nvidia.com"
    cmd = f"{email} git am --abort"
    os.system(cmd)
    cmd = f"{email} git am -q {patch_dir}/ngc_tcp_throughput.patch"
    os.system(cmd)

    cmd = "{email} git am --abort"
    os.system(cmd)
    cmd = f"{email} git am -q {patch_dir}/hbn-sleep-workaround.patch"
    os.system(cmd)

    cmd = "{email} git am --abort"
    os.system(cmd)
    cmd = f"{email} git am -q {patch_dir}/tcp-traffic-duration.patch"
    os.system(cmd)

def main():
    """
    1. apply any private patches to the sub-modules in the repo
    2. Install all the packages needed for nvcert on the test-server
    """
    #apply_patches()
    apt_install()
    pip_import_or_install("invoke")
    pip_import_or_install("decorator")
    pip_import_or_install("fabric2")
    pip_import_or_install("mako")
    pip_import_or_install("prettytable")

if __name__ == "__main__":
    main()
